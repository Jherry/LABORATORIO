﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04JuegoDados
{
    class Dado
    {
        public int valor;
        public static Random aleatorio;
        public Dado()
        {
            aleatorio = new Random();
        }
        public void TirarDado()
        {
            this.valor = aleatorio.Next(1, 6);
        }
        public void MostrarValorDado()
        {
            Console.WriteLine($"El valor del dado es: {this.valor}");
        }
    }
        class Juego
        {
            public void Jugar(Dado dado1,Dado dado2,Dado dado3)
            {
                dado1.TirarDado();
                dado1.MostrarValorDado();

                dado2.TirarDado();
                dado2.MostrarValorDado();

                dado3.TirarDado();
                dado3.MostrarValorDado();

                if (dado1.valor ==dado2.valor && dado1.valor==dado3.valor)
                {
                    Console.WriteLine("GANO");
                }
                else
                {
                    Console.WriteLine("PERDIO");
                }
            }
        }
    
    class Program
    {
        static void Main(string[] args)
        {
            Dado dado1 = new Dado();
            Dado dado2 = new Dado();
            Dado dado3 = new Dado();

            Juego j = new Juego();
            j.Jugar(dado1, dado2, dado3);

            Console.ReadKey();
        }
    }
}

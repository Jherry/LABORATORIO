﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03Banco
{
    class Cliente
    {
        public string nombre;
        public string dni;
        public string nroCuenta;
        public double saldo;

        public Cliente(string nombre, string dni, string nroCuenta, double saldo)
        {
            this.nombre = nombre;
            this.dni = dni;
            this.nroCuenta = nroCuenta;
            this.saldo = saldo;
        }

        public void RealizarDeposito(double monto)
        {
            Console.WriteLine($"Realizando DEPOSITO de {monto} soles");
            this.saldo += monto;
        }

        public void RealizarRetiro(double monto)
        {
            Console.WriteLine($"Realizando RETIRO de {monto} soles");
            this.saldo -= monto;
        }

        public void obtenerSaldoFinal()
        {
            Console.WriteLine($"Su saldo final es: {this.saldo}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ingrese su saldo Total: ");
            double saldo = Double.Parse(Console.ReadLine());

            Cliente clie01 = new Cliente("Maria", "45451245", "019236-45-1", saldo);
            Console.WriteLine($"Hola Maria, usted tiene {clie01.saldo}, realice alguna operación: ");
            Console.WriteLine("1. DEPOSITOS");
            Console.WriteLine("2. RETIROS");
            Console.Write("Seleccione opcion(1-2): ");

            int opcion = int.Parse(Console.ReadLine());

            switch (opcion)
            {
                case 1:
                    Console.Write("Ingrese monto a Depositar: ");
                    double mtoDeposito = Double.Parse(Console.ReadLine());
                    clie01.RealizarDeposito(mtoDeposito);
                    break;
                case 2:
                    Console.Write("Ingrese monto a Retirar: ");
                    double mtoRetiro = Double.Parse(Console.ReadLine());
                    clie01.RealizarRetiro(mtoRetiro);
                    break;
                default:
                    Console.WriteLine("ERROR..");
                    break;
            }

            //Realizar deposito
            clie01.obtenerSaldoFinal();
            Console.ReadKey();
        }
    }
}

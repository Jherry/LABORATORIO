﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02PromedioPOO
{
    
        class alumno
        {
            public string nombre;
            public int edad;
            public double promedio;
            public double nota1;
            public double nota2;
            public double nota3;

        public alumno(string nombre, int edad, double promedio, double nota1, double nota2, double nota3)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.promedio = promedio;
            this.nota1 = nota1;
            this.nota2 = nota2;
            this.nota3 = nota3;
        }
        public double calcularPromedio()
        {
            return (this.nota1 + this.nota2 + this.nota3) / 3;
        }
    }
    class Program
    { 
        static void Main(string[] args)
        {
            alumno alum01 = new alumno("Julio", 18, 0, 15, 16, 11);
            double promedio = alum01.calcularPromedio();
            Console.WriteLine($"El alumno{alum01.nombre} de notas: {alum01.nota1},{alum01.nota2} y{alum01.nota3} su promedio es : {promedio}");

            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01SalarioDeEmpleados
{
    class Empleado
    {
        //Atributos
        public string nombre;
        public string dni;
        public int edad;
        public int numHorasSemana;
        public double salario;

        //Metodos
        public void Trabajar(int numHorasDia)
        {
            this.numHorasSemana = numHorasDia * 5; //Lunes a Viernes
        }

        public double ObtenerSalarioSemana(double mtoXHora)
        {
            this.salario = this.numHorasSemana * mtoXHora;
            return salario;
        }

        //Constructor Anónimo porque no posee parametros
        public Empleado()
        {
        }

        //Constructor con parametros
        public Empleado(string nombre, string dni, int edad, int numHorasSemana, double salario)
        {
            this.nombre = nombre;
            this.dni = dni;
            this.edad = edad;
            this.numHorasSemana = numHorasSemana;
            this.salario = salario;
        }
    }

    class Program
    {

        static void Main(string[] args)
        {
            const double MTO_X_HORA = 120.00;
            //Crear empleado a partir del constructor
            Empleado empl01 = new Empleado(); //Construyendo con constructor anonimo
            empl01.nombre = "Carlos";
            empl01.dni = "10203040";
            empl01.edad = 25;
            empl01.salario = 0;
            empl01.numHorasSemana = 0;

            Empleado empl02 = new Empleado("David", "40251015", 18, 0, 0);
            Empleado empl03 = new Empleado("Julio", "58251402", 30, 0, 0);

            //Invocar a método trabajar
            empl01.Trabajar(5);
            empl02.Trabajar(6);
            empl03.Trabajar(8);

            empl01.ObtenerSalarioSemana(MTO_X_HORA);
            empl02.ObtenerSalarioSemana(MTO_X_HORA);
            empl03.ObtenerSalarioSemana(MTO_X_HORA);

            Console.WriteLine($"Empleado1 = {empl01.nombre} trabajó {empl01.numHorasSemana} por semana y se le pagará: {empl01.salario}");
            Console.WriteLine($"Empleado2 = {empl02.nombre} trabajó {empl02.numHorasSemana} por semana y se le pagará: {empl02.salario}");
            Console.WriteLine($"Empleado3 = {empl03.nombre} trabajó {empl03.numHorasSemana} por semana y se le pagará: {empl03.salario}");

            Console.ReadKey();
        }
    }
}

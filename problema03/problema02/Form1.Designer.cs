﻿namespace problema02
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDatosG = new System.Windows.Forms.GroupBox();
            this.grpHabilidades = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grpSueldo = new System.Windows.Forms.GroupBox();
            this.grpDatos = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.dtFecNac = new System.Windows.Forms.DateTimePicker();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.rbtnMasculino = new System.Windows.Forms.RadioButton();
            this.rbtnFemenino = new System.Windows.Forms.RadioButton();
            this.chkCantar = new System.Windows.Forms.CheckBox();
            this.chkPayaso = new System.Windows.Forms.CheckBox();
            this.chkBailar = new System.Windows.Forms.CheckBox();
            this.chkImitar = new System.Windows.Forms.CheckBox();
            this.chkMagia = new System.Windows.Forms.CheckBox();
            this.chkMalabarista = new System.Windows.Forms.CheckBox();
            this.chkMaestro = new System.Windows.Forms.CheckBox();
            this.chkOtros = new System.Windows.Forms.CheckBox();
            this.txtOtros = new System.Windows.Forms.TextBox();
            this.rbtnSin = new System.Windows.Forms.RadioButton();
            this.rbtn02 = new System.Windows.Forms.RadioButton();
            this.rbtn24 = new System.Windows.Forms.RadioButton();
            this.rbtn4 = new System.Windows.Forms.RadioButton();
            this.txtEspectativa = new System.Windows.Forms.TextBox();
            this.txtActividad = new System.Windows.Forms.TextBox();
            this.btnEvaluar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboSede = new System.Windows.Forms.ComboBox();
            this.lstPuestos = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lstPcontratar = new System.Windows.Forms.ListBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.btnCalcularMonto = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAFP = new System.Windows.Forms.TextBox();
            this.txtESSALUD = new System.Windows.Forms.TextBox();
            this.txtVida = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtEscolaridad = new System.Windows.Forms.TextBox();
            this.txtUtilidad = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.grpDatosG.SuspendLayout();
            this.grpHabilidades.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpSueldo.SuspendLayout();
            this.grpDatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDatosG
            // 
            this.grpDatosG.Controls.Add(this.rbtnFemenino);
            this.grpDatosG.Controls.Add(this.rbtnMasculino);
            this.grpDatosG.Controls.Add(this.txtDNI);
            this.grpDatosG.Controls.Add(this.dtFecNac);
            this.grpDatosG.Controls.Add(this.txtNombres);
            this.grpDatosG.Controls.Add(this.label4);
            this.grpDatosG.Controls.Add(this.label3);
            this.grpDatosG.Controls.Add(this.label2);
            this.grpDatosG.Controls.Add(this.label1);
            this.grpDatosG.Location = new System.Drawing.Point(27, 28);
            this.grpDatosG.Name = "grpDatosG";
            this.grpDatosG.Size = new System.Drawing.Size(404, 127);
            this.grpDatosG.TabIndex = 0;
            this.grpDatosG.TabStop = false;
            this.grpDatosG.Text = "Datos Generales";
            // 
            // grpHabilidades
            // 
            this.grpHabilidades.Controls.Add(this.txtOtros);
            this.grpHabilidades.Controls.Add(this.chkOtros);
            this.grpHabilidades.Controls.Add(this.chkMaestro);
            this.grpHabilidades.Controls.Add(this.chkMalabarista);
            this.grpHabilidades.Controls.Add(this.chkMagia);
            this.grpHabilidades.Controls.Add(this.chkImitar);
            this.grpHabilidades.Controls.Add(this.chkBailar);
            this.grpHabilidades.Controls.Add(this.chkPayaso);
            this.grpHabilidades.Controls.Add(this.chkCantar);
            this.grpHabilidades.Location = new System.Drawing.Point(26, 161);
            this.grpHabilidades.Name = "grpHabilidades";
            this.grpHabilidades.Size = new System.Drawing.Size(405, 165);
            this.grpHabilidades.TabIndex = 1;
            this.grpHabilidades.TabStop = false;
            this.grpHabilidades.Text = "Habilidades";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.btnEvaluar);
            this.groupBox3.Controls.Add(this.txtActividad);
            this.groupBox3.Controls.Add(this.txtEspectativa);
            this.groupBox3.Controls.Add(this.rbtn4);
            this.groupBox3.Controls.Add(this.rbtn24);
            this.groupBox3.Controls.Add(this.rbtn02);
            this.groupBox3.Controls.Add(this.rbtnSin);
            this.groupBox3.Location = new System.Drawing.Point(27, 332);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(404, 191);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Experiencia Laboral";
            // 
            // grpSueldo
            // 
            this.grpSueldo.Controls.Add(this.lblTotal);
            this.grpSueldo.Controls.Add(this.txtUtilidad);
            this.grpSueldo.Controls.Add(this.txtEscolaridad);
            this.grpSueldo.Controls.Add(this.label19);
            this.grpSueldo.Controls.Add(this.label18);
            this.grpSueldo.Controls.Add(this.txtVida);
            this.grpSueldo.Controls.Add(this.txtESSALUD);
            this.grpSueldo.Controls.Add(this.txtAFP);
            this.grpSueldo.Controls.Add(this.label17);
            this.grpSueldo.Controls.Add(this.label16);
            this.grpSueldo.Controls.Add(this.label15);
            this.grpSueldo.Controls.Add(this.label14);
            this.grpSueldo.Controls.Add(this.label13);
            this.grpSueldo.Controls.Add(this.label12);
            this.grpSueldo.Controls.Add(this.txtMonto);
            this.grpSueldo.Controls.Add(this.label11);
            this.grpSueldo.Enabled = false;
            this.grpSueldo.Location = new System.Drawing.Point(479, 332);
            this.grpSueldo.Name = "grpSueldo";
            this.grpSueldo.Size = new System.Drawing.Size(438, 191);
            this.grpSueldo.TabIndex = 3;
            this.grpSueldo.TabStop = false;
            this.grpSueldo.Text = "Sueldo Aproximado";
            // 
            // grpDatos
            // 
            this.grpDatos.Controls.Add(this.btnCalcularMonto);
            this.grpDatos.Controls.Add(this.txtSueldo);
            this.grpDatos.Controls.Add(this.label10);
            this.grpDatos.Controls.Add(this.label9);
            this.grpDatos.Controls.Add(this.btnQuitar);
            this.grpDatos.Controls.Add(this.btnAgregar);
            this.grpDatos.Controls.Add(this.lstPcontratar);
            this.grpDatos.Controls.Add(this.label8);
            this.grpDatos.Controls.Add(this.lstPuestos);
            this.grpDatos.Controls.Add(this.cboSede);
            this.grpDatos.Controls.Add(this.label7);
            this.grpDatos.Enabled = false;
            this.grpDatos.Location = new System.Drawing.Point(479, 28);
            this.grpDatos.Name = "grpDatos";
            this.grpDatos.Size = new System.Drawing.Size(438, 287);
            this.grpDatos.TabIndex = 4;
            this.grpDatos.TabStop = false;
            this.grpDatos.Text = "Datos Generales";
            this.grpDatos.Enter += new System.EventHandler(this.grpDatos_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellidos y Nombres";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha de Nacimiento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "DNI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Genero";
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(136, 24);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(233, 20);
            this.txtNombres.TabIndex = 4;
            // 
            // dtFecNac
            // 
            this.dtFecNac.Location = new System.Drawing.Point(136, 51);
            this.dtFecNac.Name = "dtFecNac";
            this.dtFecNac.Size = new System.Drawing.Size(233, 20);
            this.dtFecNac.TabIndex = 5;
            this.dtFecNac.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // txtDNI
            // 
            this.txtDNI.Location = new System.Drawing.Point(136, 78);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(134, 20);
            this.txtDNI.TabIndex = 6;
            // 
            // rbtnMasculino
            // 
            this.rbtnMasculino.AutoSize = true;
            this.rbtnMasculino.Location = new System.Drawing.Point(136, 104);
            this.rbtnMasculino.Name = "rbtnMasculino";
            this.rbtnMasculino.Size = new System.Drawing.Size(73, 17);
            this.rbtnMasculino.TabIndex = 7;
            this.rbtnMasculino.TabStop = true;
            this.rbtnMasculino.Text = "Masculino";
            this.rbtnMasculino.UseVisualStyleBackColor = true;
            // 
            // rbtnFemenino
            // 
            this.rbtnFemenino.AutoSize = true;
            this.rbtnFemenino.Location = new System.Drawing.Point(228, 104);
            this.rbtnFemenino.Name = "rbtnFemenino";
            this.rbtnFemenino.Size = new System.Drawing.Size(71, 17);
            this.rbtnFemenino.TabIndex = 8;
            this.rbtnFemenino.TabStop = true;
            this.rbtnFemenino.Text = "Femenino";
            this.rbtnFemenino.UseVisualStyleBackColor = true;
            // 
            // chkCantar
            // 
            this.chkCantar.AutoSize = true;
            this.chkCantar.Location = new System.Drawing.Point(17, 20);
            this.chkCantar.Name = "chkCantar";
            this.chkCantar.Size = new System.Drawing.Size(57, 17);
            this.chkCantar.TabIndex = 0;
            this.chkCantar.Text = "Cantar";
            this.chkCantar.UseVisualStyleBackColor = true;
            // 
            // chkPayaso
            // 
            this.chkPayaso.AutoSize = true;
            this.chkPayaso.Location = new System.Drawing.Point(17, 44);
            this.chkPayaso.Name = "chkPayaso";
            this.chkPayaso.Size = new System.Drawing.Size(61, 17);
            this.chkPayaso.TabIndex = 1;
            this.chkPayaso.Text = "Payaso";
            this.chkPayaso.UseVisualStyleBackColor = true;
            // 
            // chkBailar
            // 
            this.chkBailar.AutoSize = true;
            this.chkBailar.Location = new System.Drawing.Point(17, 68);
            this.chkBailar.Name = "chkBailar";
            this.chkBailar.Size = new System.Drawing.Size(52, 17);
            this.chkBailar.TabIndex = 2;
            this.chkBailar.Text = "Bailar";
            this.chkBailar.UseVisualStyleBackColor = true;
            // 
            // chkImitar
            // 
            this.chkImitar.AutoSize = true;
            this.chkImitar.Location = new System.Drawing.Point(17, 92);
            this.chkImitar.Name = "chkImitar";
            this.chkImitar.Size = new System.Drawing.Size(51, 17);
            this.chkImitar.TabIndex = 3;
            this.chkImitar.Text = "Imitar";
            this.chkImitar.UseVisualStyleBackColor = true;
            // 
            // chkMagia
            // 
            this.chkMagia.AutoSize = true;
            this.chkMagia.Location = new System.Drawing.Point(167, 20);
            this.chkMagia.Name = "chkMagia";
            this.chkMagia.Size = new System.Drawing.Size(87, 17);
            this.chkMagia.TabIndex = 4;
            this.chkMagia.Text = "Hacer Magia";
            this.chkMagia.UseVisualStyleBackColor = true;
            // 
            // chkMalabarista
            // 
            this.chkMalabarista.AutoSize = true;
            this.chkMalabarista.Location = new System.Drawing.Point(167, 44);
            this.chkMalabarista.Name = "chkMalabarista";
            this.chkMalabarista.Size = new System.Drawing.Size(80, 17);
            this.chkMalabarista.TabIndex = 5;
            this.chkMalabarista.Text = "Malabarista";
            this.chkMalabarista.UseVisualStyleBackColor = true;
            // 
            // chkMaestro
            // 
            this.chkMaestro.AutoSize = true;
            this.chkMaestro.Location = new System.Drawing.Point(167, 68);
            this.chkMaestro.Name = "chkMaestro";
            this.chkMaestro.Size = new System.Drawing.Size(131, 17);
            this.chkMaestro.TabIndex = 6;
            this.chkMaestro.Text = "Maestro de ceremonia";
            this.chkMaestro.UseVisualStyleBackColor = true;
            // 
            // chkOtros
            // 
            this.chkOtros.AutoSize = true;
            this.chkOtros.Location = new System.Drawing.Point(167, 92);
            this.chkOtros.Name = "chkOtros";
            this.chkOtros.Size = new System.Drawing.Size(115, 17);
            this.chkOtros.TabIndex = 7;
            this.chkOtros.Text = "Otros (Especifique)";
            this.chkOtros.UseVisualStyleBackColor = true;
            // 
            // txtOtros
            // 
            this.txtOtros.Location = new System.Drawing.Point(188, 124);
            this.txtOtros.Name = "txtOtros";
            this.txtOtros.Size = new System.Drawing.Size(182, 20);
            this.txtOtros.TabIndex = 8;
            // 
            // rbtnSin
            // 
            this.rbtnSin.AutoSize = true;
            this.rbtnSin.Location = new System.Drawing.Point(16, 31);
            this.rbtnSin.Name = "rbtnSin";
            this.rbtnSin.Size = new System.Drawing.Size(97, 17);
            this.rbtnSin.TabIndex = 0;
            this.rbtnSin.TabStop = true;
            this.rbtnSin.Text = "Sin experiencia";
            this.rbtnSin.UseVisualStyleBackColor = true;
            // 
            // rbtn02
            // 
            this.rbtn02.AutoSize = true;
            this.rbtn02.Location = new System.Drawing.Point(16, 55);
            this.rbtn02.Name = "rbtn02";
            this.rbtn02.Size = new System.Drawing.Size(66, 17);
            this.rbtn02.TabIndex = 1;
            this.rbtn02.TabStop = true;
            this.rbtn02.Text = "0-2 años";
            this.rbtn02.UseVisualStyleBackColor = true;
            this.rbtn02.CheckedChanged += new System.EventHandler(this.rbtn02_CheckedChanged);
            // 
            // rbtn24
            // 
            this.rbtn24.AutoSize = true;
            this.rbtn24.Location = new System.Drawing.Point(16, 79);
            this.rbtn24.Name = "rbtn24";
            this.rbtn24.Size = new System.Drawing.Size(66, 17);
            this.rbtn24.TabIndex = 2;
            this.rbtn24.TabStop = true;
            this.rbtn24.Text = "2-4 años";
            this.rbtn24.UseVisualStyleBackColor = true;
            this.rbtn24.CheckedChanged += new System.EventHandler(this.rbtn24_CheckedChanged);
            // 
            // rbtn4
            // 
            this.rbtn4.AutoSize = true;
            this.rbtn4.Location = new System.Drawing.Point(16, 103);
            this.rbtn4.Name = "rbtn4";
            this.rbtn4.Size = new System.Drawing.Size(94, 17);
            this.rbtn4.TabIndex = 3;
            this.rbtn4.TabStop = true;
            this.rbtn4.Text = "mas de 4 años";
            this.rbtn4.UseVisualStyleBackColor = true;
            this.rbtn4.CheckedChanged += new System.EventHandler(this.rbtn4_CheckedChanged);
            // 
            // txtEspectativa
            // 
            this.txtEspectativa.Location = new System.Drawing.Point(154, 49);
            this.txtEspectativa.Name = "txtEspectativa";
            this.txtEspectativa.Size = new System.Drawing.Size(215, 20);
            this.txtEspectativa.TabIndex = 4;
            // 
            // txtActividad
            // 
            this.txtActividad.Enabled = false;
            this.txtActividad.Location = new System.Drawing.Point(154, 104);
            this.txtActividad.Name = "txtActividad";
            this.txtActividad.Size = new System.Drawing.Size(215, 20);
            this.txtActividad.TabIndex = 5;
            this.txtActividad.TextChanged += new System.EventHandler(this.txtActividad_TextChanged);
            // 
            // btnEvaluar
            // 
            this.btnEvaluar.Location = new System.Drawing.Point(126, 148);
            this.btnEvaluar.Name = "btnEvaluar";
            this.btnEvaluar.Size = new System.Drawing.Size(127, 23);
            this.btnEvaluar.TabIndex = 6;
            this.btnEvaluar.Text = "Evaluar Registro";
            this.btnEvaluar.UseVisualStyleBackColor = true;
            this.btnEvaluar.Click += new System.EventHandler(this.btnEvaluar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(154, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Ingrese expectativa Salarial";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Ultima actividad realizada";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Sede";
            // 
            // cboSede
            // 
            this.cboSede.FormattingEnabled = true;
            this.cboSede.Items.AddRange(new object[] {
            "Plaza Norte",
            "Real Plaza",
            "Mall Sur"});
            this.cboSede.Location = new System.Drawing.Point(102, 24);
            this.cboSede.Name = "cboSede";
            this.cboSede.Size = new System.Drawing.Size(241, 21);
            this.cboSede.TabIndex = 1;
            this.cboSede.SelectedIndexChanged += new System.EventHandler(this.cboSede_SelectedIndexChanged);
            // 
            // lstPuestos
            // 
            this.lstPuestos.FormattingEnabled = true;
            this.lstPuestos.Location = new System.Drawing.Point(10, 91);
            this.lstPuestos.Name = "lstPuestos";
            this.lstPuestos.ScrollAlwaysVisible = true;
            this.lstPuestos.Size = new System.Drawing.Size(120, 121);
            this.lstPuestos.TabIndex = 2;
            this.lstPuestos.SelectedIndexChanged += new System.EventHandler(this.lstPuestos_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Puestos Requeridos";
            // 
            // lstPcontratar
            // 
            this.lstPcontratar.FormattingEnabled = true;
            this.lstPcontratar.Location = new System.Drawing.Point(239, 91);
            this.lstPcontratar.Name = "lstPcontratar";
            this.lstPcontratar.Size = new System.Drawing.Size(193, 121);
            this.lstPcontratar.TabIndex = 4;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(148, 121);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 5;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(148, 160);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(75, 23);
            this.btnQuitar.TabIndex = 6;
            this.btnQuitar.Text = "Quitar";
            this.btnQuitar.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(239, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Puestos a Contratar";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 239);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Sueldo";
            // 
            // txtSueldo
            // 
            this.txtSueldo.Location = new System.Drawing.Point(66, 239);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(100, 20);
            this.txtSueldo.TabIndex = 9;
            this.txtSueldo.TextChanged += new System.EventHandler(this.txtSueldo_TextChanged);
            // 
            // btnCalcularMonto
            // 
            this.btnCalcularMonto.Location = new System.Drawing.Point(239, 239);
            this.btnCalcularMonto.Name = "btnCalcularMonto";
            this.btnCalcularMonto.Size = new System.Drawing.Size(193, 23);
            this.btnCalcularMonto.TabIndex = 10;
            this.btnCalcularMonto.Text = "Calcular Monto a Pagar";
            this.btnCalcularMonto.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Monto:";
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(56, 31);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(142, 20);
            this.txtMonto.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(213, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "nuevos soles";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Descuentos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(241, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Beneficios:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "AFP(12%)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 129);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "ESSALUD(10%)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 157);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Vida Ley(5%)";
            // 
            // txtAFP
            // 
            this.txtAFP.Location = new System.Drawing.Point(115, 97);
            this.txtAFP.Name = "txtAFP";
            this.txtAFP.Size = new System.Drawing.Size(100, 20);
            this.txtAFP.TabIndex = 8;
            this.txtAFP.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // txtESSALUD
            // 
            this.txtESSALUD.Location = new System.Drawing.Point(115, 129);
            this.txtESSALUD.Name = "txtESSALUD";
            this.txtESSALUD.Size = new System.Drawing.Size(100, 20);
            this.txtESSALUD.TabIndex = 9;
            // 
            // txtVida
            // 
            this.txtVida.Location = new System.Drawing.Point(115, 157);
            this.txtVida.Name = "txtVida";
            this.txtVida.Size = new System.Drawing.Size(100, 20);
            this.txtVida.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(244, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "Escolaridad(8%)";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(244, 129);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Utilidad(15%)";
            // 
            // txtEscolaridad
            // 
            this.txtEscolaridad.Location = new System.Drawing.Point(332, 97);
            this.txtEscolaridad.Name = "txtEscolaridad";
            this.txtEscolaridad.Size = new System.Drawing.Size(100, 20);
            this.txtEscolaridad.TabIndex = 13;
            // 
            // txtUtilidad
            // 
            this.txtUtilidad.Location = new System.Drawing.Point(332, 129);
            this.txtUtilidad.Name = "txtUtilidad";
            this.txtUtilidad.Size = new System.Drawing.Size(100, 20);
            this.txtUtilidad.TabIndex = 14;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(247, 163);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(57, 13);
            this.lblTotal.TabIndex = 15;
            this.lblTotal.Text = "Total Neto";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 593);
            this.Controls.Add(this.grpDatos);
            this.Controls.Add(this.grpSueldo);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpHabilidades);
            this.Controls.Add(this.grpDatosG);
            this.Name = "Form1";
            this.Text = "Convocatoria circo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpDatosG.ResumeLayout(false);
            this.grpDatosG.PerformLayout();
            this.grpHabilidades.ResumeLayout(false);
            this.grpHabilidades.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpSueldo.ResumeLayout(false);
            this.grpSueldo.PerformLayout();
            this.grpDatos.ResumeLayout(false);
            this.grpDatos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDatosG;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpHabilidades;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox grpSueldo;
        private System.Windows.Forms.GroupBox grpDatos;
        private System.Windows.Forms.RadioButton rbtnFemenino;
        private System.Windows.Forms.RadioButton rbtnMasculino;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.DateTimePicker dtFecNac;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtOtros;
        private System.Windows.Forms.CheckBox chkOtros;
        private System.Windows.Forms.CheckBox chkMaestro;
        private System.Windows.Forms.CheckBox chkMalabarista;
        private System.Windows.Forms.CheckBox chkMagia;
        private System.Windows.Forms.CheckBox chkImitar;
        private System.Windows.Forms.CheckBox chkBailar;
        private System.Windows.Forms.CheckBox chkPayaso;
        private System.Windows.Forms.CheckBox chkCantar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEvaluar;
        private System.Windows.Forms.TextBox txtActividad;
        private System.Windows.Forms.TextBox txtEspectativa;
        private System.Windows.Forms.RadioButton rbtn4;
        private System.Windows.Forms.RadioButton rbtn24;
        private System.Windows.Forms.RadioButton rbtn02;
        private System.Windows.Forms.RadioButton rbtnSin;
        private System.Windows.Forms.TextBox txtVida;
        private System.Windows.Forms.TextBox txtESSALUD;
        private System.Windows.Forms.TextBox txtAFP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCalcularMonto;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.ListBox lstPcontratar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lstPuestos;
        private System.Windows.Forms.ComboBox cboSede;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtUtilidad;
        private System.Windows.Forms.TextBox txtEscolaridad;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _09RegistroComensales
{
    public partial class frmRegistro : Form

    {
        static string[] ArrayNomComensales = new string[5];
        static string[] ArrayIdComensales = new string[5];
        static string[] ArrayCargosComensales = new string[5];

        public static void cargarComensalesDB()
        {
            ArrayNomComensales[0] = "Alan Garcia";
            ArrayNomComensales[1] = "Renato Rossini";
            ArrayNomComensales[2] = "Laura Bosso";
            ArrayNomComensales[3] = "Alumno";
            ArrayNomComensales[4] = "Alumno";

            ArrayCargosComensales[0] = "Alumno";
            ArrayCargosComensales[1] = "Instructor";
            ArrayCargosComensales[2] = "Administrativo";
            ArrayCargosComensales[3] = "Alumno";
            ArrayCargosComensales[4] = "Alumno";

            ArrayIdComensales[0] = "ID001";
            ArrayIdComensales[1] = "ID002";
            ArrayIdComensales[2] = "ID003";
            ArrayIdComensales[3] = "ID004";
            ArrayIdComensales[4] = "ID005";

        }
        
        
        public string buscarComensal(string id)
        {
            string resultado = string.Empty;
            for(int i=0;i<ArrayIdComensales.Length;i++)
            {
                if (id.Equals(ArrayIdComensales[i]))
                {
                    resultado = $"{ArrayIdComensales[i]} - {ArrayNomComensales[i]} - {ArrayCargosComensales[i]}";
                        break;
                }
            }
            return resultado;
        }
        public frmRegistro()
        {
            InitializeComponent();
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void frmRegistro_Load(object sender, EventArgs e)
        {
            grpOferta.Visible = false;
            grpMenu.Visible = false;
            grpExtras.Visible = false;
            txtComensal.Enabled = false;
            txtID.Focus();
            cargarComensalesDB();
            
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string idComensal = txtID.Text;
            if(idComensal.Equals(string.Empty))
            {
                MessageBox.Show("Debe ingresar el ID a consultar", "INFORMACION");
            }
            else if(buscarComensal(idComensal).Equals(string.Empty))
             {
                    MessageBox.Show($"El usuario de ID:{idComensal} no esta registrado en nuestro sistema", "Mensaje");
                }
            else
            {
                txtComensal.Text = buscarComensal(idComensal.ToUpper());//Buscar con mayusuculas
                grpOferta.Visible = true;

            }
            
            }

        private void rbtMenu_CheckedChanged(object sender, EventArgs e)
        {
            if(rbtMenu.Checked)
            {
                grpMenu.Visible = true;
            }
            else
            {
                grpMenu.Visible = false;
            }
        }

        private void rbtExtra_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtExtra.Checked)
            {
                grpExtras.Visible = true;
            }
            else
            {
                grpExtras.Visible = false;
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            }

        private void btnAgregarE_Click(object sender, EventArgs e)
        {
            lstMenuElegido.Items.Add(lstEntrada.SelectedItem);
        }

        private void btnAgregarS_Click(object sender, EventArgs e)
        {
            lstMenuElegido.Items.Add(lstSegundo.SelectedItem);
        }

        private void btnAgregarP_Click(object sender, EventArgs e)
        {
            lstMenuElegido.Items.Add(lstPostres.SelectedItem);
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double montototal = 0;
            if(rbtMenu.Checked)
            {
                montototal = 10.00;
            }
            else if(rbtExtra.Checked)
            {
                //Calcular y sumar platos seleccionados
                if(chkTallarin.Checked)
                {
                    lstMenuElegido.Items.Add("Tallarines");
                    montototal = montototal + 10;
                }
                if (chkLomo.Checked)
                {
                    lstMenuElegido.Items.Add("LomoSaltado");
                    montototal = montototal + 15;
                }
                if (chkGordon.Checked)
                {
                    lstMenuElegido.Items.Add("GordonBlue");
                    montototal = montototal + 20;
                }
                if (chkPachamanca.Checked)
                {
                    lstMenuElegido.Items.Add("Pachamanca");
                    montototal = montototal + 25;
                }
                if (chkGaseosa.Checked)
                {
                    lstMenuElegido.Items.Add("Gaseosa");
                    montototal = montototal + 3;
                }
                if (chkCerveza.Checked)
                {
                    lstMenuElegido.Items.Add("Cerveza");
                    montototal = montototal + 7;
                }

            }
            lblMontototal.Text = $"Total a pagar:{montototal.ToString("c2")}";
        }
    }
    }
    


﻿namespace _09RegistroComensales
{
    partial class frmRegistro
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtComensal = new System.Windows.Forms.TextBox();
            this.grpOferta = new System.Windows.Forms.GroupBox();
            this.grpExtras = new System.Windows.Forms.GroupBox();
            this.lstEntrada = new System.Windows.Forms.ListBox();
            this.lstSegundo = new System.Windows.Forms.ListBox();
            this.lstPostres = new System.Windows.Forms.ListBox();
            this.lstMenuElegido = new System.Windows.Forms.ListBox();
            this.lblMontototal = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnAgregarP = new System.Windows.Forms.Button();
            this.btnAgregarS = new System.Windows.Forms.Button();
            this.btnAgregarE = new System.Windows.Forms.Button();
            this.chkTallarin = new System.Windows.Forms.CheckBox();
            this.chkLomo = new System.Windows.Forms.CheckBox();
            this.chkGordon = new System.Windows.Forms.CheckBox();
            this.chkPachamanca = new System.Windows.Forms.CheckBox();
            this.chkGaseosa = new System.Windows.Forms.CheckBox();
            this.chkCerveza = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtMenu = new System.Windows.Forms.RadioButton();
            this.rbtExtra = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.grpMenu = new System.Windows.Forms.GroupBox();
            this.grpOferta.SuspendLayout();
            this.grpExtras.SuspendLayout();
            this.grpMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese ID";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(81, 26);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 1;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(199, 26);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtComensal
            // 
            this.txtComensal.Location = new System.Drawing.Point(339, 25);
            this.txtComensal.Name = "txtComensal";
            this.txtComensal.Size = new System.Drawing.Size(280, 20);
            this.txtComensal.TabIndex = 3;
            // 
            // grpOferta
            // 
            this.grpOferta.Controls.Add(this.rbtExtra);
            this.grpOferta.Controls.Add(this.rbtMenu);
            this.grpOferta.Location = new System.Drawing.Point(25, 63);
            this.grpOferta.Name = "grpOferta";
            this.grpOferta.Size = new System.Drawing.Size(594, 51);
            this.grpOferta.TabIndex = 4;
            this.grpOferta.TabStop = false;
            this.grpOferta.Text = "Ofertas";
            // 
            // grpExtras
            // 
            this.grpExtras.Controls.Add(this.chkCerveza);
            this.grpExtras.Controls.Add(this.chkGaseosa);
            this.grpExtras.Controls.Add(this.chkPachamanca);
            this.grpExtras.Controls.Add(this.chkGordon);
            this.grpExtras.Controls.Add(this.chkLomo);
            this.grpExtras.Controls.Add(this.chkTallarin);
            this.grpExtras.Location = new System.Drawing.Point(340, 120);
            this.grpExtras.Name = "grpExtras";
            this.grpExtras.Size = new System.Drawing.Size(279, 159);
            this.grpExtras.TabIndex = 5;
            this.grpExtras.TabStop = false;
            this.grpExtras.Text = "Platos Extras";
            // 
            // lstEntrada
            // 
            this.lstEntrada.FormattingEnabled = true;
            this.lstEntrada.Items.AddRange(new object[] {
            "Ceviche",
            "Ocopa",
            "Papa a la Huancaina",
            "Tequeños",
            "Ensalada"});
            this.lstEntrada.Location = new System.Drawing.Point(63, 10);
            this.lstEntrada.Name = "lstEntrada";
            this.lstEntrada.Size = new System.Drawing.Size(120, 95);
            this.lstEntrada.TabIndex = 6;
            // 
            // lstSegundo
            // 
            this.lstSegundo.FormattingEnabled = true;
            this.lstSegundo.Items.AddRange(new object[] {
            "Pollo al Vino",
            "Estofado de Carne",
            "Tallarines verdes",
            "Chuleta frita",
            "Ensalada Rusa"});
            this.lstSegundo.Location = new System.Drawing.Point(63, 111);
            this.lstSegundo.Name = "lstSegundo";
            this.lstSegundo.Size = new System.Drawing.Size(120, 95);
            this.lstSegundo.TabIndex = 7;
            // 
            // lstPostres
            // 
            this.lstPostres.FormattingEnabled = true;
            this.lstPostres.Items.AddRange(new object[] {
            "Torta de Chocolate",
            "Leche Asada",
            "Mazamorra",
            "Flan con Gelatina",
            "Empanada"});
            this.lstPostres.Location = new System.Drawing.Point(63, 212);
            this.lstPostres.Name = "lstPostres";
            this.lstPostres.Size = new System.Drawing.Size(120, 95);
            this.lstPostres.TabIndex = 8;
            // 
            // lstMenuElegido
            // 
            this.lstMenuElegido.FormattingEnabled = true;
            this.lstMenuElegido.Location = new System.Drawing.Point(346, 311);
            this.lstMenuElegido.Name = "lstMenuElegido";
            this.lstMenuElegido.ScrollAlwaysVisible = true;
            this.lstMenuElegido.Size = new System.Drawing.Size(181, 95);
            this.lstMenuElegido.TabIndex = 9;
            this.lstMenuElegido.SelectedIndexChanged += new System.EventHandler(this.listBox4_SelectedIndexChanged);
            // 
            // lblMontototal
            // 
            this.lblMontototal.AutoSize = true;
            this.lblMontototal.Location = new System.Drawing.Point(537, 315);
            this.lblMontototal.Name = "lblMontototal";
            this.lblMontototal.Size = new System.Drawing.Size(73, 13);
            this.lblMontototal.TabIndex = 10;
            this.lblMontototal.Text = "Total a pagar:";
            this.lblMontototal.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(346, 412);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 11;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(427, 412);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 12;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(508, 412);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 13;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            // 
            // btnAgregarP
            // 
            this.btnAgregarP.Location = new System.Drawing.Point(203, 250);
            this.btnAgregarP.Name = "btnAgregarP";
            this.btnAgregarP.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarP.TabIndex = 14;
            this.btnAgregarP.Text = "AgregarPostre";
            this.btnAgregarP.UseVisualStyleBackColor = true;
            this.btnAgregarP.Click += new System.EventHandler(this.btnAgregarP_Click);
            // 
            // btnAgregarS
            // 
            this.btnAgregarS.Location = new System.Drawing.Point(203, 146);
            this.btnAgregarS.Name = "btnAgregarS";
            this.btnAgregarS.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarS.TabIndex = 15;
            this.btnAgregarS.Text = "AgregarSegundo";
            this.btnAgregarS.UseVisualStyleBackColor = true;
            this.btnAgregarS.Click += new System.EventHandler(this.btnAgregarS_Click);
            // 
            // btnAgregarE
            // 
            this.btnAgregarE.Location = new System.Drawing.Point(203, 42);
            this.btnAgregarE.Name = "btnAgregarE";
            this.btnAgregarE.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarE.TabIndex = 16;
            this.btnAgregarE.Text = "AgregarEntrada";
            this.btnAgregarE.UseVisualStyleBackColor = true;
            this.btnAgregarE.Click += new System.EventHandler(this.btnAgregarE_Click);
            // 
            // chkTallarin
            // 
            this.chkTallarin.AutoSize = true;
            this.chkTallarin.Location = new System.Drawing.Point(6, 19);
            this.chkTallarin.Name = "chkTallarin";
            this.chkTallarin.Size = new System.Drawing.Size(150, 17);
            this.chkTallarin.TabIndex = 0;
            this.chkTallarin.Text = "Tallarin Saltado -(S/10.00)";
            this.chkTallarin.UseVisualStyleBackColor = true;
            // 
            // chkLomo
            // 
            this.chkLomo.AutoSize = true;
            this.chkLomo.Location = new System.Drawing.Point(6, 42);
            this.chkLomo.Name = "chkLomo";
            this.chkLomo.Size = new System.Drawing.Size(140, 17);
            this.chkLomo.TabIndex = 1;
            this.chkLomo.Text = "Lomo saltado -(S/15.00)";
            this.chkLomo.UseVisualStyleBackColor = true;
            this.chkLomo.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chkGordon
            // 
            this.chkGordon.AutoSize = true;
            this.chkGordon.Location = new System.Drawing.Point(6, 65);
            this.chkGordon.Name = "chkGordon";
            this.chkGordon.Size = new System.Drawing.Size(136, 17);
            this.chkGordon.TabIndex = 2;
            this.chkGordon.Text = "Gordon Blue -(S/20.00)";
            this.chkGordon.UseVisualStyleBackColor = true;
            // 
            // chkPachamanca
            // 
            this.chkPachamanca.AutoSize = true;
            this.chkPachamanca.Location = new System.Drawing.Point(6, 88);
            this.chkPachamanca.Name = "chkPachamanca";
            this.chkPachamanca.Size = new System.Drawing.Size(140, 17);
            this.chkPachamanca.TabIndex = 3;
            this.chkPachamanca.Text = "Pachamanca -(S/25.00)";
            this.chkPachamanca.UseVisualStyleBackColor = true;
            // 
            // chkGaseosa
            // 
            this.chkGaseosa.AutoSize = true;
            this.chkGaseosa.Location = new System.Drawing.Point(6, 111);
            this.chkGaseosa.Name = "chkGaseosa";
            this.chkGaseosa.Size = new System.Drawing.Size(138, 17);
            this.chkGaseosa.TabIndex = 4;
            this.chkGaseosa.Text = "Gaseosa Pers. -(s/3.00)";
            this.chkGaseosa.UseVisualStyleBackColor = true;
            // 
            // chkCerveza
            // 
            this.chkCerveza.AutoSize = true;
            this.chkCerveza.Location = new System.Drawing.Point(6, 131);
            this.chkCerveza.Name = "chkCerveza";
            this.chkCerveza.Size = new System.Drawing.Size(135, 17);
            this.chkCerveza.TabIndex = 5;
            this.chkCerveza.Text = "Cerveza Pers. -(s/7.00)";
            this.chkCerveza.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(346, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Menú elegido:";
            // 
            // rbtMenu
            // 
            this.rbtMenu.AutoSize = true;
            this.rbtMenu.Location = new System.Drawing.Point(56, 20);
            this.rbtMenu.Name = "rbtMenu";
            this.rbtMenu.Size = new System.Drawing.Size(163, 17);
            this.rbtMenu.TabIndex = 0;
            this.rbtMenu.TabStop = true;
            this.rbtMenu.Text = "Menú del día (S/10.00 soles)";
            this.rbtMenu.UseVisualStyleBackColor = true;
            this.rbtMenu.CheckedChanged += new System.EventHandler(this.rbtMenu_CheckedChanged);
            // 
            // rbtExtra
            // 
            this.rbtExtra.AutoSize = true;
            this.rbtExtra.Location = new System.Drawing.Point(385, 20);
            this.rbtExtra.Name = "rbtExtra";
            this.rbtExtra.Size = new System.Drawing.Size(86, 17);
            this.rbtExtra.TabIndex = 1;
            this.rbtExtra.TabStop = true;
            this.rbtExtra.Text = "Platos Extras";
            this.rbtExtra.UseVisualStyleBackColor = true;
            this.rbtExtra.CheckedChanged += new System.EventHandler(this.rbtExtra_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Postres:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Segundo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Entrada";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // grpMenu
            // 
            this.grpMenu.Controls.Add(this.lstEntrada);
            this.grpMenu.Controls.Add(this.label6);
            this.grpMenu.Controls.Add(this.btnAgregarP);
            this.grpMenu.Controls.Add(this.btnAgregarS);
            this.grpMenu.Controls.Add(this.btnAgregarE);
            this.grpMenu.Controls.Add(this.lstSegundo);
            this.grpMenu.Controls.Add(this.label5);
            this.grpMenu.Controls.Add(this.lstPostres);
            this.grpMenu.Controls.Add(this.label4);
            this.grpMenu.Location = new System.Drawing.Point(25, 120);
            this.grpMenu.Name = "grpMenu";
            this.grpMenu.Size = new System.Drawing.Size(293, 322);
            this.grpMenu.TabIndex = 21;
            this.grpMenu.TabStop = false;
            this.grpMenu.Text = "Menú del Día";
            // 
            // frmRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 450);
            this.Controls.Add(this.grpMenu);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.grpExtras);
            this.Controls.Add(this.grpOferta);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.txtComensal);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lstMenuElegido);
            this.Controls.Add(this.lblMontototal);
            this.Name = "frmRegistro";
            this.Text = "Registro de comensales SENATI";
            this.Load += new System.EventHandler(this.frmRegistro_Load);
            this.grpOferta.ResumeLayout(false);
            this.grpOferta.PerformLayout();
            this.grpExtras.ResumeLayout(false);
            this.grpExtras.PerformLayout();
            this.grpMenu.ResumeLayout(false);
            this.grpMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtComensal;
        private System.Windows.Forms.GroupBox grpOferta;
        private System.Windows.Forms.GroupBox grpExtras;
        private System.Windows.Forms.CheckBox chkGaseosa;
        private System.Windows.Forms.CheckBox chkPachamanca;
        private System.Windows.Forms.CheckBox chkGordon;
        private System.Windows.Forms.CheckBox chkLomo;
        private System.Windows.Forms.CheckBox chkTallarin;
        private System.Windows.Forms.ListBox lstEntrada;
        private System.Windows.Forms.ListBox lstSegundo;
        private System.Windows.Forms.ListBox lstPostres;
        private System.Windows.Forms.ListBox lstMenuElegido;
        private System.Windows.Forms.Label lblMontototal;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnAgregarP;
        private System.Windows.Forms.Button btnAgregarS;
        private System.Windows.Forms.Button btnAgregarE;
        private System.Windows.Forms.CheckBox chkCerveza;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbtExtra;
        private System.Windows.Forms.RadioButton rbtMenu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox grpMenu;
    }
}


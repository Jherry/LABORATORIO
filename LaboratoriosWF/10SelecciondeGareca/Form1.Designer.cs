﻿namespace _10_SeleccionGareca
{
    partial class frmSeleccion
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboPosicion = new System.Windows.Forms.ComboBox();
            this.lstJugadores = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lstEquipo = new System.Windows.Forms.ListBox();
            this.lblMensaje = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione posicion para jugadores";
            // 
            // cboPosicion
            // 
            this.cboPosicion.FormattingEnabled = true;
            this.cboPosicion.Location = new System.Drawing.Point(65, 69);
            this.cboPosicion.Name = "cboPosicion";
            this.cboPosicion.Size = new System.Drawing.Size(172, 21);
            this.cboPosicion.TabIndex = 1;
            this.cboPosicion.SelectedIndexChanged += new System.EventHandler(this.cboPosicion_SelectedIndexChanged);
            // 
            // lstJugadores
            // 
            this.lstJugadores.FormattingEnabled = true;
            this.lstJugadores.Location = new System.Drawing.Point(65, 136);
            this.lstJugadores.Name = "lstJugadores";
            this.lstJugadores.Size = new System.Drawing.Size(172, 121);
            this.lstJugadores.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(306, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = ">>";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(306, 216);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "<<";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // lstEquipo
            // 
            this.lstEquipo.FormattingEnabled = true;
            this.lstEquipo.Location = new System.Drawing.Point(408, 136);
            this.lstEquipo.Name = "lstEquipo";
            this.lstEquipo.Size = new System.Drawing.Size(190, 121);
            this.lstEquipo.TabIndex = 5;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Location = new System.Drawing.Point(65, 304);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(533, 134);
            this.lblMensaje.TabIndex = 6;
            this.lblMensaje.TabStop = false;
            this.lblMensaje.Text = "Mensaje de confirmacion";
            // 
            // frmSeleccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 450);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.lstEquipo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lstJugadores);
            this.Controls.Add(this.cboPosicion);
            this.Controls.Add(this.label1);
            this.Name = "frmSeleccion";
            this.Text = "Seleccion de Gareca";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboPosicion;
        private System.Windows.Forms.ListBox lstJugadores;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox lstEquipo;
        private System.Windows.Forms.GroupBox lblMensaje;
    }
}


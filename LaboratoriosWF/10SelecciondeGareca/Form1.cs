﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _10_SeleccionGareca
{
    public partial class frmSeleccion : Form
    {
        static string[] arrayNomJugadores = new string[24];
        static string[] arrayPosicionJugadores = new string[4];

        static void cargarJugadoresDB()
        {
            //ARQUEROS
            arrayNomJugadores[0] = "Pedro Gallese";
            arrayNomJugadores[1] = "Carlos Cáceda";
            arrayNomJugadores[2] = "José Carvallo";
            //DEFENSAS
            arrayNomJugadores[3] = "Aldo Corzo";
            arrayNomJugadores[4] = "Luis Advíncula";
            arrayNomJugadores[5] = "Christian Ramos";
            arrayNomJugadores[6] = "Miguel Araujo";
            arrayNomJugadores[7] = "Alberto Rodríguez";
            arrayNomJugadores[8] = "Anderson Santamaría";
            arrayNomJugadores[9] = "Miguel Trauco";
            arrayNomJugadores[10] = "Nilson Loyola";
            arrayNomJugadores[11] = "Luis Abram";
            //MEDIOCAMPISTAS
            arrayNomJugadores[12] = "Renato Tapia";
            arrayNomJugadores[13] = "Pedro Aquino";
            arrayNomJugadores[14] = "Yoshimar Yotún";
            arrayNomJugadores[15] = "Wilder Cartagena";
            arrayNomJugadores[16] = "Sergio Peña";
            arrayNomJugadores[17] = "Christian Cueva";
            arrayNomJugadores[18] = "Edison Flores";
            arrayNomJugadores[19] = "Paolo Hurtado";
            arrayNomJugadores[20] = "André Carrillo";
            arrayNomJugadores[21] = "Andy Polo";
            //DELANTEROS
            arrayNomJugadores[22] = "Jefferson Farfán";
            arrayNomJugadores[23] = "Raúl Ruidíaz";

            arrayPosicionJugadores[0] = "ARQUEROS";
            arrayPosicionJugadores[1] = "DEFENSAS";
            arrayPosicionJugadores[2] = "MEDIOCAMPISTAS";
            arrayPosicionJugadores[3] = "DELANTEROS";
        }
        public frmSeleccion()
        {
            InitializeComponent();
        }

        private void frmSeleccion_Load(object sender, EventArgs e)
        {
            cargarJugadoresDB();
            for (int i = 0; i < arrayPosicionJugadores.Length; i++)
            {
                cboPosicion.Items.Add(arrayPosicionJugadores[i]);
            }
        }

        private void cboPosicion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPosicion.SelectedIndex == -1)
            {
                MessageBox.Show("Debe posicionar una poicion", "Mensaje");
            }
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            lstEquipo.Items.Add(lstJugadores.SelectedItem);
            if (lstEquipo.Items.Count >= 5)
            {
                lblMensaje.Text = "Equipo completo... A GANAR!!!";
            }

        }
    }
}
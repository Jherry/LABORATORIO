﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01SaludoDeBienvenida
{
    public partial class frmBienvenida : Form
    {
        public frmBienvenida()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text;
            if(nombre.Equals("") || nombre.Length<=2)
            {
                MessageBox.Show($"Debera ingresar un nombre para proceder","ERROR");
            }
            lblBienvenido.Text = "USTED SE ENCUENTRA EN EL IV SEMESTRE SENATI 2018";
            MessageBox.Show($"Bienvenido  {nombre.ToUpper()}");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }
    }
}

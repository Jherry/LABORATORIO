﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03CalculadoraEstandar
{
    public partial class FrmCalculadora : Form
    {
        static string numero1 = string.Empty;
        static string numero2 = string.Empty;
        static char operador = ' ';

        public string esCero(string valor)
        {
            string resultado = string.Empty;
            if(valor.Equals("0"))
            {
                resultado = "0";
            }
            else
            {
                resultado = valor + "0";
            }
            return resultado;
        }
        public FrmCalculadora()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            txtResultado.Text = esCero(txtResultado.Text);
        }
        private void btnUno_Click(object sender, EventArgs e)
        {

            if(esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "1";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "1";
            }
        }

        private void btnTres_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "3";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "3";
            }
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "4";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "4";
            }
        }

        private void btnDos_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "2";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "2";
            }
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "5";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "5";
            }
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "6";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "6";
            }
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "7";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "7";
            }
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "8";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "8";
            }
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {

            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "9";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + "9";
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtResultado.Text = "0";
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            numero1 = txtResultado.Text;
            txtResultado.Text = "0";
            operador = '+';
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            numero2= txtResultado.Text;
            double resultado = 0;
            switch(operador)
            {
                case '+':
                    resultado = double.Parse(numero1) + double.Parse(numero2);
                    break;
                case '-':
                    resultado = double.Parse(numero1) - double.Parse(numero2);
                    break;
                case 'x':
                    resultado = double.Parse(numero1) * double.Parse(numero2);
                    break;
                case '/':
                    resultado = double.Parse(numero1) / double.Parse(numero2);
                    break;
                default:
                    break;
            }
            txtResultado.Text = resultado.ToString();
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            numero1 = txtResultado.Text;
            txtResultado.Text = "0";
            operador = '-';
        }

        private void btnMultiplicacion_Click(object sender, EventArgs e)
        {
            numero1 = txtResultado.Text;
            txtResultado.Text = "0";
            operador = 'x';
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            numero1 = txtResultado.Text;
            txtResultado.Text = "0";
            operador = '/';
        }

        private void btnRaiz_Click(object sender, EventArgs e)
        {
            int valor = int.Parse(txtResultado.Text);
            double resultado = Math.Sqrt(valor);
            txtResultado.Text = resultado.ToString();

        }

        private void btnPunto_Click(object sender, EventArgs e)
        {
            if (esCero(txtResultado.Text).Equals("0"))
            {
                txtResultado.Text = "0"+".";
            }
            else
            {
                txtResultado.Text = txtResultado.Text + ".";
            }
        }
    }
}

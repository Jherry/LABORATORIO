﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08EncuestaNovio
{
    public partial class frmEncuestaNovio : Form
    {
        static int edad = 0;
        public frmEncuestaNovio()
        {
            InitializeComponent();
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void rbtEgresado_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void frmEncuestaNovio_Load(object sender, EventArgs e)
        {
            cboEducacion.Items.Add("SENATI");
            cboEducacion.Items.Add("ICPNA");
            cboEducacion.Items.Add("BRITANICO");
            grpLugarTrabajo.Visible = false;
        }

        private void dFecNac_ValueChanged(object sender, EventArgs e)
        {
           int AñoActual = DateTime.Now.Year;
            int AñoSeleccionado = dFecNac.Value.Year;
            edad = AñoActual - AñoSeleccionado;

            lblEdad.Text = $"Su Edad es:{edad} años";
            lblEdad.Visible=true;
            if(edad<18)
            {
                MessageBox.Show("Usted no tiene edad para esto");
                btnEvaluar.Enabled = false;
            }
            else
            {
                btnEvaluar.Enabled = true;
            }

        }

        private void btnEvaluar_Click(object sender, EventArgs e)
        {
            if (txtApellido.Text.Equals(string.Empty) && txtNombre.Text.Equals(string.Empty))
            {
                MessageBox.Show("Los apellidos y nombres deben estar completos");
            }
            else if (edad<18)
            {
                MessageBox.Show("Debe ingresar su edad", "faltan datos");
            }

            else if (cboEducacion.SelectedIndex == -1)
            {
                MessageBox.Show("Debe seleccionar institucion de formacion", "faltan datos");
            }
            else if (!rbtEstudiante.Checked && !rbtEgresado.Checked)
            {
                MessageBox.Show("Debe seleccionar su nivel de educacion", "faltan datos");
            }
            else if (!rbtOpcionSi.Checked && rbtOpcionNo.Checked || !rbtOpcionOtro.Checked)
            {
                MessageBox.Show("Debe seleccionar su ocupacion", "faltan datos");
            }
            else if (!rbtSi.Checked && !rbtNo.Checked)
            {
                MessageBox.Show("Debe informar si posee vehiculo", "faltan datos");
            }
            else
            {
                MessageBox.Show("Correcto");
            }
            
        }

        private void rbtEstudiante_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbtOpcionSi_CheckedChanged(object sender, EventArgs e)
        {
            if(rbtOpcionSi.Checked)
            {
                grpLugarTrabajo.Visible = true;
            }
            else
            {
                grpLugarTrabajo.Visible = false;
            }
        }
    }
}

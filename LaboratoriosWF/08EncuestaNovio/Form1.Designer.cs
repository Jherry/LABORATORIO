﻿namespace _08EncuestaNovio
{
    partial class frmEncuestaNovio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtLugarTrabajo = new System.Windows.Forms.TextBox();
            this.txtIngresoMensual = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.cboEducacion = new System.Windows.Forms.ComboBox();
            this.rbtEstudiante = new System.Windows.Forms.RadioButton();
            this.rbtEgresado = new System.Windows.Forms.RadioButton();
            this.dFecNac = new System.Windows.Forms.DateTimePicker();
            this.btnEvaluar = new System.Windows.Forms.Button();
            this.chkJugarFutbol = new System.Windows.Forms.CheckBox();
            this.chkAlCine = new System.Windows.Forms.CheckBox();
            this.chkACaminar = new System.Windows.Forms.CheckBox();
            this.chkJugarDota = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbtOpcionSi = new System.Windows.Forms.RadioButton();
            this.rbtOpcionNo = new System.Windows.Forms.RadioButton();
            this.rbtOpcionOtro = new System.Windows.Forms.RadioButton();
            this.rbtSi = new System.Windows.Forms.RadioButton();
            this.rbtNo = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.grpLugarTrabajo = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.grpLugarTrabajo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dFecNac);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.txtApellido);
            this.groupBox1.Controls.Add(this.lblEdad);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(204, 149);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbtEgresado);
            this.groupBox2.Controls.Add(this.rbtEstudiante);
            this.groupBox2.Controls.Add(this.cboEducacion);
            this.groupBox2.Location = new System.Drawing.Point(241, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 149);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Educacion";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.chkJugarDota);
            this.groupBox3.Controls.Add(this.chkACaminar);
            this.groupBox3.Controls.Add(this.chkAlCine);
            this.groupBox3.Controls.Add(this.chkJugarFutbol);
            this.groupBox3.Location = new System.Drawing.Point(12, 190);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(204, 180);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hobbies";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbtOpcionOtro);
            this.groupBox4.Controls.Add(this.rbtOpcionNo);
            this.groupBox4.Controls.Add(this.rbtOpcionSi);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(241, 190);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 180);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ocupacion";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbtNo);
            this.groupBox5.Controls.Add(this.rbtSi);
            this.groupBox5.Location = new System.Drawing.Point(470, 318);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 52);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tienes Carro?";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // txtLugarTrabajo
            // 
            this.txtLugarTrabajo.Location = new System.Drawing.Point(27, 27);
            this.txtLugarTrabajo.Name = "txtLugarTrabajo";
            this.txtLugarTrabajo.Size = new System.Drawing.Size(154, 20);
            this.txtLugarTrabajo.TabIndex = 5;
            // 
            // txtIngresoMensual
            // 
            this.txtIngresoMensual.Location = new System.Drawing.Point(27, 86);
            this.txtIngresoMensual.Name = "txtIngresoMensual";
            this.txtIngresoMensual.Size = new System.Drawing.Size(117, 20);
            this.txtIngresoMensual.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellidos:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombres:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "F. de Nac.";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(9, 118);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(64, 13);
            this.lblEdad.TabIndex = 3;
            this.lblEdad.Text = "Tu edad es:";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(76, 34);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(100, 20);
            this.txtApellido.TabIndex = 4;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(76, 62);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 5;
            // 
            // cboEducacion
            // 
            this.cboEducacion.FormattingEnabled = true;
            this.cboEducacion.Location = new System.Drawing.Point(17, 33);
            this.cboEducacion.Name = "cboEducacion";
            this.cboEducacion.Size = new System.Drawing.Size(152, 21);
            this.cboEducacion.TabIndex = 0;
            // 
            // rbtEstudiante
            // 
            this.rbtEstudiante.AutoSize = true;
            this.rbtEstudiante.Location = new System.Drawing.Point(17, 86);
            this.rbtEstudiante.Name = "rbtEstudiante";
            this.rbtEstudiante.Size = new System.Drawing.Size(75, 17);
            this.rbtEstudiante.TabIndex = 1;
            this.rbtEstudiante.TabStop = true;
            this.rbtEstudiante.Text = "Estudiante";
            this.rbtEstudiante.UseVisualStyleBackColor = true;
            this.rbtEstudiante.CheckedChanged += new System.EventHandler(this.rbtEstudiante_CheckedChanged);
            // 
            // rbtEgresado
            // 
            this.rbtEgresado.AutoSize = true;
            this.rbtEgresado.Location = new System.Drawing.Point(99, 88);
            this.rbtEgresado.Name = "rbtEgresado";
            this.rbtEgresado.Size = new System.Drawing.Size(70, 17);
            this.rbtEgresado.TabIndex = 2;
            this.rbtEgresado.TabStop = true;
            this.rbtEgresado.Text = "Egresado";
            this.rbtEgresado.UseVisualStyleBackColor = true;
            this.rbtEgresado.CheckedChanged += new System.EventHandler(this.rbtEgresado_CheckedChanged);
            // 
            // dFecNac
            // 
            this.dFecNac.Location = new System.Drawing.Point(76, 86);
            this.dFecNac.Name = "dFecNac";
            this.dFecNac.Size = new System.Drawing.Size(100, 20);
            this.dFecNac.TabIndex = 6;
            this.dFecNac.Value = new System.DateTime(2018, 8, 27, 11, 36, 27, 0);
            this.dFecNac.ValueChanged += new System.EventHandler(this.dFecNac_ValueChanged);
            // 
            // btnEvaluar
            // 
            this.btnEvaluar.Enabled = false;
            this.btnEvaluar.Location = new System.Drawing.Point(537, 90);
            this.btnEvaluar.Name = "btnEvaluar";
            this.btnEvaluar.Size = new System.Drawing.Size(75, 23);
            this.btnEvaluar.TabIndex = 7;
            this.btnEvaluar.Text = "Evaluar";
            this.btnEvaluar.UseVisualStyleBackColor = true;
            this.btnEvaluar.Click += new System.EventHandler(this.btnEvaluar_Click);
            // 
            // chkJugarFutbol
            // 
            this.chkJugarFutbol.AutoSize = true;
            this.chkJugarFutbol.Location = new System.Drawing.Point(12, 32);
            this.chkJugarFutbol.Name = "chkJugarFutbol";
            this.chkJugarFutbol.Size = new System.Drawing.Size(87, 17);
            this.chkJugarFutbol.TabIndex = 0;
            this.chkJugarFutbol.Text = "Jugar Futbol.";
            this.chkJugarFutbol.UseVisualStyleBackColor = true;
            // 
            // chkAlCine
            // 
            this.chkAlCine.AutoSize = true;
            this.chkAlCine.Location = new System.Drawing.Point(12, 56);
            this.chkAlCine.Name = "chkAlCine";
            this.chkAlCine.Size = new System.Drawing.Size(67, 17);
            this.chkAlCine.TabIndex = 1;
            this.chkAlCine.Text = "Ir al Cine";
            this.chkAlCine.UseVisualStyleBackColor = true;
            // 
            // chkACaminar
            // 
            this.chkACaminar.AutoSize = true;
            this.chkACaminar.Location = new System.Drawing.Point(12, 80);
            this.chkACaminar.Name = "chkACaminar";
            this.chkACaminar.Size = new System.Drawing.Size(67, 17);
            this.chkACaminar.TabIndex = 2;
            this.chkACaminar.Text = "Caminar.";
            this.chkACaminar.UseVisualStyleBackColor = true;
            // 
            // chkJugarDota
            // 
            this.chkJugarDota.AutoSize = true;
            this.chkJugarDota.Location = new System.Drawing.Point(12, 104);
            this.chkJugarDota.Name = "chkJugarDota";
            this.chkJugarDota.Size = new System.Drawing.Size(90, 17);
            this.chkJugarDota.TabIndex = 3;
            this.chkJugarDota.Text = "Jugar Dota 2.";
            this.chkJugarDota.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Seleccione sus opciones.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Trabaja?";
            // 
            // rbtOpcionSi
            // 
            this.rbtOpcionSi.AutoSize = true;
            this.rbtOpcionSi.Location = new System.Drawing.Point(17, 56);
            this.rbtOpcionSi.Name = "rbtOpcionSi";
            this.rbtOpcionSi.Size = new System.Drawing.Size(37, 17);
            this.rbtOpcionSi.TabIndex = 1;
            this.rbtOpcionSi.TabStop = true;
            this.rbtOpcionSi.Text = "Si.";
            this.rbtOpcionSi.UseVisualStyleBackColor = true;
            this.rbtOpcionSi.CheckedChanged += new System.EventHandler(this.rbtOpcionSi_CheckedChanged);
            // 
            // rbtOpcionNo
            // 
            this.rbtOpcionNo.AutoSize = true;
            this.rbtOpcionNo.Location = new System.Drawing.Point(17, 80);
            this.rbtOpcionNo.Name = "rbtOpcionNo";
            this.rbtOpcionNo.Size = new System.Drawing.Size(42, 17);
            this.rbtOpcionNo.TabIndex = 2;
            this.rbtOpcionNo.TabStop = true;
            this.rbtOpcionNo.Text = "No.";
            this.rbtOpcionNo.UseVisualStyleBackColor = true;
            // 
            // rbtOpcionOtro
            // 
            this.rbtOpcionOtro.AutoSize = true;
            this.rbtOpcionOtro.Location = new System.Drawing.Point(17, 104);
            this.rbtOpcionOtro.Name = "rbtOpcionOtro";
            this.rbtOpcionOtro.Size = new System.Drawing.Size(87, 17);
            this.rbtOpcionOtro.TabIndex = 3;
            this.rbtOpcionOtro.TabStop = true;
            this.rbtOpcionOtro.Text = "Me recurseo.";
            this.rbtOpcionOtro.UseVisualStyleBackColor = true;
            // 
            // rbtSi
            // 
            this.rbtSi.AutoSize = true;
            this.rbtSi.Location = new System.Drawing.Point(11, 22);
            this.rbtSi.Name = "rbtSi";
            this.rbtSi.Size = new System.Drawing.Size(34, 17);
            this.rbtSi.TabIndex = 0;
            this.rbtSi.TabStop = true;
            this.rbtSi.Text = "Si";
            this.rbtSi.UseVisualStyleBackColor = true;
            // 
            // rbtNo
            // 
            this.rbtNo.AutoSize = true;
            this.rbtNo.Location = new System.Drawing.Point(102, 22);
            this.rbtNo.Name = "rbtNo";
            this.rbtNo.Size = new System.Drawing.Size(39, 17);
            this.rbtNo.TabIndex = 1;
            this.rbtNo.TabStop = true;
            this.rbtNo.Text = "No";
            this.rbtNo.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ingresos Mensuales?";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // grpLugarTrabajo
            // 
            this.grpLugarTrabajo.Controls.Add(this.label7);
            this.grpLugarTrabajo.Controls.Add(this.txtLugarTrabajo);
            this.grpLugarTrabajo.Controls.Add(this.txtIngresoMensual);
            this.grpLugarTrabajo.Location = new System.Drawing.Point(470, 182);
            this.grpLugarTrabajo.Name = "grpLugarTrabajo";
            this.grpLugarTrabajo.Size = new System.Drawing.Size(200, 129);
            this.grpLugarTrabajo.TabIndex = 10;
            this.grpLugarTrabajo.TabStop = false;
            this.grpLugarTrabajo.Text = "Lugar de Trabajo:";
            // 
            // frmEncuestaNovio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 450);
            this.Controls.Add(this.grpLugarTrabajo);
            this.Controls.Add(this.btnEvaluar);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmEncuestaNovio";
            this.Text = "Encuesta para futuro novio";
            this.Load += new System.EventHandler(this.frmEncuestaNovio_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.grpLugarTrabajo.ResumeLayout(false);
            this.grpLugarTrabajo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtLugarTrabajo;
        private System.Windows.Forms.TextBox txtIngresoMensual;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtEgresado;
        private System.Windows.Forms.RadioButton rbtEstudiante;
        private System.Windows.Forms.ComboBox cboEducacion;
        private System.Windows.Forms.DateTimePicker dFecNac;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkJugarDota;
        private System.Windows.Forms.CheckBox chkACaminar;
        private System.Windows.Forms.CheckBox chkAlCine;
        private System.Windows.Forms.CheckBox chkJugarFutbol;
        private System.Windows.Forms.RadioButton rbtOpcionOtro;
        private System.Windows.Forms.RadioButton rbtOpcionNo;
        private System.Windows.Forms.RadioButton rbtOpcionSi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbtNo;
        private System.Windows.Forms.RadioButton rbtSi;
        private System.Windows.Forms.Button btnEvaluar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox grpLugarTrabajo;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05Logueo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text;
            string pass = txtClave.Text;

            const string USUARIO_REAL = "Senati";
            const string CLAVE_REAL = "iestp2021";

            if(String.IsNullOrEmpty(usuario))
            {
                MessageBox.Show("Usuario incorrecto, vuelva a intentarlo");
            }
            else if (String.IsNullOrEmpty(pass))
            {
                MessageBox.Show("Contraseña incorrecto, vuelva a intentarlo");
            }
            else if (!usuario.Equals(USUARIO_REAL))
            {
                MessageBox.Show("El usuario no existe en nuestra base de datos");
            }
            else if (!pass.Equals(CLAVE_REAL))
            {
                MessageBox.Show("La contraseña no existe en nuestra base de datos");
            }
            else
            {
                MessageBox.Show("!INGRESO EXITOSO!");
            }

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblTitulo_Click(object sender, EventArgs e)
        {

        }
    }
}

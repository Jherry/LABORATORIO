﻿namespace _05IngresoCine
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPeliculas = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkPopcorn = new System.Windows.Forms.CheckBox();
            this.chkGaseosa = new System.Windows.Forms.CheckBox();
            this.chkCafe = new System.Windows.Forms.CheckBox();
            this.txtPopcorn = new System.Windows.Forms.TextBox();
            this.txtGaseosa = new System.Windows.Forms.TextBox();
            this.txtCafe = new System.Windows.Forms.TextBox();
            this.chkNiño = new System.Windows.Forms.CheckBox();
            this.chkAdulto = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCantEntradasNiños = new System.Windows.Forms.TextBox();
            this.txtCantEntradasAdultos = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optSocioSi = new System.Windows.Forms.RadioButton();
            this.optSocioNo = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSaludo = new System.Windows.Forms.Label();
            this.lblSala = new System.Windows.Forms.Label();
            this.lblMonto = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellidos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Edad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(264, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombres";
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(89, 27);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(100, 20);
            this.txtApellidos.TabIndex = 3;
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(337, 27);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(100, 20);
            this.txtNombres.TabIndex = 4;
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(89, 71);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(68, 20);
            this.txtEdad.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(200, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Es socio? :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCantEntradasAdultos);
            this.groupBox1.Controls.Add(this.txtCantEntradasNiños);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chkAdulto);
            this.groupBox1.Controls.Add(this.chkNiño);
            this.groupBox1.Controls.Add(this.cboPeliculas);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(23, 126);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 143);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Entradas";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCafe);
            this.groupBox2.Controls.Add(this.txtGaseosa);
            this.groupBox2.Controls.Add(this.txtPopcorn);
            this.groupBox2.Controls.Add(this.chkCafe);
            this.groupBox2.Controls.Add(this.chkGaseosa);
            this.groupBox2.Controls.Add(this.chkPopcorn);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(302, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(209, 143);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compras";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Seleccione pelicula";
            // 
            // cboPeliculas
            // 
            this.cboPeliculas.FormattingEnabled = true;
            this.cboPeliculas.Location = new System.Drawing.Point(13, 36);
            this.cboPeliculas.Name = "cboPeliculas";
            this.cboPeliculas.Size = new System.Drawing.Size(121, 21);
            this.cboPeliculas.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(152, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Cantidad";
            // 
            // chkPopcorn
            // 
            this.chkPopcorn.AutoSize = true;
            this.chkPopcorn.Location = new System.Drawing.Point(7, 43);
            this.chkPopcorn.Name = "chkPopcorn";
            this.chkPopcorn.Size = new System.Drawing.Size(151, 17);
            this.chkPopcorn.TabIndex = 1;
            this.chkPopcorn.Text = "Popcorn mediana($ 10.00)";
            this.chkPopcorn.UseVisualStyleBackColor = true;
            // 
            // chkGaseosa
            // 
            this.chkGaseosa.AutoSize = true;
            this.chkGaseosa.Location = new System.Drawing.Point(7, 67);
            this.chkGaseosa.Name = "chkGaseosa";
            this.chkGaseosa.Size = new System.Drawing.Size(147, 17);
            this.chkGaseosa.TabIndex = 2;
            this.chkGaseosa.Text = "Gaseosa personal ($3.00)";
            this.chkGaseosa.UseVisualStyleBackColor = true;
            // 
            // chkCafe
            // 
            this.chkCafe.AutoSize = true;
            this.chkCafe.Location = new System.Drawing.Point(7, 91);
            this.chkCafe.Name = "chkCafe";
            this.chkCafe.Size = new System.Drawing.Size(127, 17);
            this.chkCafe.TabIndex = 3;
            this.chkCafe.Text = "Café personal ($2.00)";
            this.chkCafe.UseVisualStyleBackColor = true;
            // 
            // txtPopcorn
            // 
            this.txtPopcorn.Enabled = false;
            this.txtPopcorn.Location = new System.Drawing.Point(154, 43);
            this.txtPopcorn.Name = "txtPopcorn";
            this.txtPopcorn.Size = new System.Drawing.Size(46, 20);
            this.txtPopcorn.TabIndex = 4;
            // 
            // txtGaseosa
            // 
            this.txtGaseosa.Enabled = false;
            this.txtGaseosa.Location = new System.Drawing.Point(154, 67);
            this.txtGaseosa.Name = "txtGaseosa";
            this.txtGaseosa.Size = new System.Drawing.Size(46, 20);
            this.txtGaseosa.TabIndex = 5;
            // 
            // txtCafe
            // 
            this.txtCafe.Enabled = false;
            this.txtCafe.Location = new System.Drawing.Point(154, 91);
            this.txtCafe.Name = "txtCafe";
            this.txtCafe.Size = new System.Drawing.Size(46, 20);
            this.txtCafe.TabIndex = 6;
            // 
            // chkNiño
            // 
            this.chkNiño.AutoSize = true;
            this.chkNiño.Location = new System.Drawing.Point(13, 84);
            this.chkNiño.Name = "chkNiño";
            this.chkNiño.Size = new System.Drawing.Size(48, 17);
            this.chkNiño.TabIndex = 2;
            this.chkNiño.Text = "Niño";
            this.chkNiño.UseVisualStyleBackColor = true;
            this.chkNiño.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // chkAdulto
            // 
            this.chkAdulto.AutoSize = true;
            this.chkAdulto.Location = new System.Drawing.Point(13, 116);
            this.chkAdulto.Name = "chkAdulto";
            this.chkAdulto.Size = new System.Drawing.Size(56, 17);
            this.chkAdulto.TabIndex = 3;
            this.chkAdulto.Text = "Adulto";
            this.chkAdulto.UseVisualStyleBackColor = true;
            this.chkAdulto.CheckedChanged += new System.EventHandler(this.chkAdulto_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(102, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Cantidad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(155, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "$25.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(156, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "$30.00";
            // 
            // txtCantEntradasNiños
            // 
            this.txtCantEntradasNiños.Enabled = false;
            this.txtCantEntradasNiños.Location = new System.Drawing.Point(105, 84);
            this.txtCantEntradasNiños.Name = "txtCantEntradasNiños";
            this.txtCantEntradasNiños.Size = new System.Drawing.Size(46, 20);
            this.txtCantEntradasNiños.TabIndex = 7;
            // 
            // txtCantEntradasAdultos
            // 
            this.txtCantEntradasAdultos.Enabled = false;
            this.txtCantEntradasAdultos.Location = new System.Drawing.Point(105, 112);
            this.txtCantEntradasAdultos.Name = "txtCantEntradasAdultos";
            this.txtCantEntradasAdultos.Size = new System.Drawing.Size(46, 20);
            this.txtCantEntradasAdultos.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblMonto);
            this.groupBox3.Controls.Add(this.lblSala);
            this.groupBox3.Controls.Add(this.lblSaludo);
            this.groupBox3.Location = new System.Drawing.Point(23, 286);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(488, 152);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Total a pagar";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // optSocioSi
            // 
            this.optSocioSi.AutoSize = true;
            this.optSocioSi.Location = new System.Drawing.Point(267, 73);
            this.optSocioSi.Name = "optSocioSi";
            this.optSocioSi.Size = new System.Drawing.Size(35, 17);
            this.optSocioSi.TabIndex = 10;
            this.optSocioSi.TabStop = true;
            this.optSocioSi.Text = "SI";
            this.optSocioSi.UseVisualStyleBackColor = true;
            this.optSocioSi.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // optSocioNo
            // 
            this.optSocioNo.AutoSize = true;
            this.optSocioNo.Location = new System.Drawing.Point(308, 73);
            this.optSocioNo.Name = "optSocioNo";
            this.optSocioNo.Size = new System.Drawing.Size(41, 17);
            this.optSocioNo.TabIndex = 11;
            this.optSocioNo.TabStop = true;
            this.optSocioNo.Text = "NO";
            this.optSocioNo.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(352, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "(Descuento 20% por ser un socio)";
            // 
            // lblSaludo
            // 
            this.lblSaludo.AutoSize = true;
            this.lblSaludo.Location = new System.Drawing.Point(13, 31);
            this.lblSaludo.Name = "lblSaludo";
            this.lblSaludo.Size = new System.Drawing.Size(136, 13);
            this.lblSaludo.TabIndex = 0;
            this.lblSaludo.Text = "Selecciona tus productos...";
            // 
            // lblSala
            // 
            this.lblSala.AutoSize = true;
            this.lblSala.Location = new System.Drawing.Point(13, 65);
            this.lblSala.Name = "lblSala";
            this.lblSala.Size = new System.Drawing.Size(136, 13);
            this.lblSala.TabIndex = 1;
            this.lblSala.Text = "Selecciona tus productos...";
            // 
            // lblMonto
            // 
            this.lblMonto.AutoSize = true;
            this.lblMonto.Location = new System.Drawing.Point(16, 102);
            this.lblMonto.Name = "lblMonto";
            this.lblMonto.Size = new System.Drawing.Size(136, 13);
            this.lblMonto.TabIndex = 2;
            this.lblMonto.Text = "Selecciona tus productos...";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(224, 200);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 13;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 450);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.optSocioNo);
            this.Controls.Add(this.optSocioSi);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtNombres);
            this.Controls.Add(this.txtApellidos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Registro de personas al cine";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkNiño;
        private System.Windows.Forms.ComboBox cboPeliculas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCafe;
        private System.Windows.Forms.TextBox txtGaseosa;
        private System.Windows.Forms.TextBox txtPopcorn;
        private System.Windows.Forms.CheckBox chkCafe;
        private System.Windows.Forms.CheckBox chkGaseosa;
        private System.Windows.Forms.CheckBox chkPopcorn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCantEntradasAdultos;
        private System.Windows.Forms.TextBox txtCantEntradasNiños;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkAdulto;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton optSocioSi;
        private System.Windows.Forms.Label lblMonto;
        private System.Windows.Forms.Label lblSala;
        private System.Windows.Forms.Label lblSaludo;
        private System.Windows.Forms.RadioButton optSocioNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnCalcular;
    }
}


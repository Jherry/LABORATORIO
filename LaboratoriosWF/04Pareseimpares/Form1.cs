﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04Pareseimpares
{
    public partial class Form1 : Form
    {
        public bool esPar(int num)
        {
            bool resultado = false;
            if (num%2==0)
            {
                resultado = true;
            }
            return resultado;
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            int valor = int.Parse(txtNumero.Text);
            if (esPar(valor))
            {
                //agregar a lstPares
                lstPares.Items.Add(valor);
            }
            else //agregar a lstImpares
            {
                lstImpares.Items.Add(valor);
            }
            txtNumero.Clear();
            txtNumero.Focus();
        }

        private void txtCanPares_TextChanged(object sender, EventArgs e)
        {

        }

        private void grpreporte_Enter(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int numItemsPares = lstPares.Items.Count;
            int numItemsImpares = lstImpares.Items.Count;
            int sumaPares = 0;
            int sumaImpares = 0;

            if(numItemsPares>0)
            {
                for(int i=0;i<numItemsPares;i++)
                {
                    sumaPares = sumaPares + int.Parse(lstPares.Items[i].ToString());
                }

                if (numItemsImpares > 0)
                {
                    for (int i = 0; i < numItemsImpares; i++)
                    {
                        sumaImpares = sumaImpares + int.Parse(lstImpares.Items[i].ToString());
                    }
                }
                    txtCantImpares.Text = sumaImpares.ToString();
                    txtCanPares.Text = sumaPares.ToString();
                txtSumaNumeros.Text = (sumaImpares + sumaPares).ToString();
                }

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNumero.Clear();
            txtCantImpares.Clear();
            txtCanPares.Clear();
            txtSumaNumeros.Clear();
            lstPares.Items.Clear();
            lstImpares.Items.Clear();
        
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            

            int indicePar = lstPares.SelectedIndex;
            
            int numItemsPares = lstPares.Items.Count;
            

            if (numItemsPares <= 0)
            {
                //lstPares esta vacia
                MessageBox.Show("La lista de pares esta vacia,ingrese valores", "ERROR");
            }
            else
            {

                if (indicePar == -1)
                {
                    //lstPares esta vacia
                    MessageBox.Show("La lista de pares esta vacia,ingrese valores,", "ERROR");
                }
                else
                {
                    lstPares.Items.RemoveAt(lstPares.SelectedIndex);
                }
            }

            


            lstPares.Items.RemoveAt(lstPares.SelectedIndex);
            lstImpares.Items.RemoveAt(lstImpares.SelectedIndex);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int indiceImpar = lstImpares.SelectedIndex;
            int numItemsImpares = lstImpares.Items.Count;

            if (numItemsImpares <= 0)
            {
                //lstPares esta vacia
                MessageBox.Show("La lista de impares esta vacia,ingrese valores", "ERROR");
            }
            else
            {

                if (indiceImpar == -1)
                {
                    //lstPares esta vacia
                    MessageBox.Show("La lista de impares esta vacia,ingrese valores,", "ERROR");
                }
                else
                {
                    lstImpares.Items.RemoveAt(lstImpares.SelectedIndex);
                }
            }

        }
    }
}

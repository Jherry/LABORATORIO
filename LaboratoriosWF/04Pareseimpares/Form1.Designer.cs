﻿namespace _04Pareseimpares
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstPares = new System.Windows.Forms.ListBox();
            this.lstImpares = new System.Windows.Forms.ListBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnEliminarPar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.grpreporte = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCanPares = new System.Windows.Forms.TextBox();
            this.txtCantImpares = new System.Windows.Forms.TextBox();
            this.txtSumaNumeros = new System.Windows.Forms.TextBox();
            this.btnEliminarImpar = new System.Windows.Forms.Button();
            this.grpreporte.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese número";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(114, 31);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(88, 20);
            this.txtNumero.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lista de números pares";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(238, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lista de números impares";
            // 
            // lstPares
            // 
            this.lstPares.FormattingEnabled = true;
            this.lstPares.Location = new System.Drawing.Point(26, 131);
            this.lstPares.Name = "lstPares";
            this.lstPares.Size = new System.Drawing.Size(113, 121);
            this.lstPares.TabIndex = 4;
            // 
            // lstImpares
            // 
            this.lstImpares.FormattingEnabled = true;
            this.lstImpares.Location = new System.Drawing.Point(241, 131);
            this.lstImpares.Name = "lstImpares";
            this.lstImpares.Size = new System.Drawing.Size(123, 121);
            this.lstImpares.TabIndex = 5;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(155, 125);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 6;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEliminarPar
            // 
            this.btnEliminarPar.Location = new System.Drawing.Point(155, 179);
            this.btnEliminarPar.Name = "btnEliminarPar";
            this.btnEliminarPar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarPar.TabIndex = 7;
            this.btnEliminarPar.Text = "Elim Pares";
            this.btnEliminarPar.UseVisualStyleBackColor = true;
            this.btnEliminarPar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(155, 205);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 8;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(155, 231);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 9;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // grpreporte
            // 
            this.grpreporte.Controls.Add(this.txtSumaNumeros);
            this.grpreporte.Controls.Add(this.txtCantImpares);
            this.grpreporte.Controls.Add(this.txtCanPares);
            this.grpreporte.Controls.Add(this.label6);
            this.grpreporte.Controls.Add(this.label5);
            this.grpreporte.Controls.Add(this.label4);
            this.grpreporte.Location = new System.Drawing.Point(26, 272);
            this.grpreporte.Name = "grpreporte";
            this.grpreporte.Size = new System.Drawing.Size(338, 154);
            this.grpreporte.TabIndex = 10;
            this.grpreporte.TabStop = false;
            this.grpreporte.Text = "Reporte Contador";
            this.grpreporte.Enter += new System.EventHandler(this.grpreporte_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Cantidad Pares:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Cantidad Impares:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Suma de Números:";
            // 
            // txtCanPares
            // 
            this.txtCanPares.Enabled = false;
            this.txtCanPares.Location = new System.Drawing.Point(147, 34);
            this.txtCanPares.Name = "txtCanPares";
            this.txtCanPares.Size = new System.Drawing.Size(128, 20);
            this.txtCanPares.TabIndex = 5;
            this.txtCanPares.TextChanged += new System.EventHandler(this.txtCanPares_TextChanged);
            // 
            // txtCantImpares
            // 
            this.txtCantImpares.Enabled = false;
            this.txtCantImpares.Location = new System.Drawing.Point(147, 64);
            this.txtCantImpares.Name = "txtCantImpares";
            this.txtCantImpares.Size = new System.Drawing.Size(128, 20);
            this.txtCantImpares.TabIndex = 6;
            // 
            // txtSumaNumeros
            // 
            this.txtSumaNumeros.Enabled = false;
            this.txtSumaNumeros.Location = new System.Drawing.Point(147, 95);
            this.txtSumaNumeros.Name = "txtSumaNumeros";
            this.txtSumaNumeros.Size = new System.Drawing.Size(128, 20);
            this.txtSumaNumeros.TabIndex = 7;
            // 
            // btnEliminarImpar
            // 
            this.btnEliminarImpar.Location = new System.Drawing.Point(155, 151);
            this.btnEliminarImpar.Name = "btnEliminarImpar";
            this.btnEliminarImpar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarImpar.TabIndex = 11;
            this.btnEliminarImpar.Text = "Elim Impares";
            this.btnEliminarImpar.UseVisualStyleBackColor = true;
            this.btnEliminarImpar.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 450);
            this.Controls.Add(this.btnEliminarImpar);
            this.Controls.Add(this.grpreporte);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnEliminarPar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.lstImpares);
            this.Controls.Add(this.lstPares);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Ingreso y calculo de números";
            this.grpreporte.ResumeLayout(false);
            this.grpreporte.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstPares;
        private System.Windows.Forms.ListBox lstImpares;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEliminarPar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.GroupBox grpreporte;
        private System.Windows.Forms.TextBox txtSumaNumeros;
        private System.Windows.Forms.TextBox txtCantImpares;
        private System.Windows.Forms.TextBox txtCanPares;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnEliminarImpar;
    }
}


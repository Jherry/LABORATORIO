﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _07SorteoFIFA
{
    public partial class frmSorteoFIFA : Form
    {
        static int NUM_EQUIPOS_X_GRUPO=3;
        public void activarOpcionAgregar()
        {
            btnAgregar.Enabled = true;
            txtEquipo.Enabled = true;
        }
        public void desactivarOpcionAgregar()
        {
            btnAgregar.Enabled = false;
            txtEquipo.Enabled = false;
        }
        public frmSorteoFIFA()
        {
            InitializeComponent();
        }

        private void txtEquipo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string equipo = txtEquipo.Text;
            if(equipo.Equals(String.Empty))
            {
                MessageBox.Show("", "Incidenca de Ingreso");
            }
            else
            {
                if (rbtGrupoA.Checked)
                {
                    if (lstGrupoA.Items.Count >= NUM_EQUIPOS_X_GRUPO)
                    {
                        MessageBox.Show("Ya no puedes agregar mas equipos al GrupoA.", "Limite de Ingreso");
                    }
                    else
                    {
                        lstGrupoA.Items.Add(equipo);
                    }


                }
                else if (rbtGrupoB.Checked)
                {
                    if (lstGrupoB.Items.Count >= NUM_EQUIPOS_X_GRUPO)
                    {
                        MessageBox.Show("Ya no puedes agregar mas equipos al GrupoB.", "Limite de Ingreso");
                    }
                    else
                    {
                        lstGrupoB.Items.Add(equipo);
                    }

                }
                else if (rbtGrupoC.Checked)
                {
                    if (lstGrupoC.Items.Count >= NUM_EQUIPOS_X_GRUPO)
                    {
                        MessageBox.Show("Ya no puedes agregar mas equipos al GrupoC.", "Limite de Ingreso");
                    }
                    else
                    {
                    }
                    lstGrupoC.Items.Add(equipo);
                }
                else if (rbtGrupoD.Checked)
                {
                    if (lstGrupoD.Items.Count >= NUM_EQUIPOS_X_GRUPO)
                    {
                        MessageBox.Show("Ya no puedes agregar mas equipos al GrupoD.", "Limite de Ingreso");
                    }
                    else
                    {

                        lstGrupoD.Items.Add(equipo);

                    }
                }
                txtEquipo.Clear();
                txtEquipo.Focus();
                if (lstGrupoA.Items.Count == NUM_EQUIPOS_X_GRUPO && lstGrupoB.Items.Count == NUM_EQUIPOS_X_GRUPO && lstGrupoC.Items.Count == NUM_EQUIPOS_X_GRUPO && lstGrupoD.Items.Count == NUM_EQUIPOS_X_GRUPO) 
                {
                    btnIniciarSorteo.Enabled = true;
                }
            }
        }

        private void frmSorteoFIFA_Load(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            btnIniciarSorteo.Enabled = false;
            txtEquipo.Enabled = false;
        }

        private void rbtGrupoA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtGrupoA.Checked)
            {


                activarOpcionAgregar();
            }
            else
            {
                desactivarOpcionAgregar();
            }
        }

        private void rbtGrupoB_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtGrupoB.Checked)
            {


                activarOpcionAgregar();
            }
            else
            {
                desactivarOpcionAgregar();
            }
        }

        private void rbtGrupoC_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtGrupoC.Checked)
            {


                activarOpcionAgregar();
            }
            else
            {
                desactivarOpcionAgregar();
            }
        }

        private void rbtGrupoD_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtGrupoD.Checked)
            {


                activarOpcionAgregar();
            }
            else
            {
                desactivarOpcionAgregar();
            }
        }

        private void btnIniciarSorteo_Click(object sender, EventArgs e)
        {
            if (rbtGrupoA.Checked)
            {
                //Sorteo de Grupo a
                Random rnd = new Random();
                int indiceEquipo1 = rnd.Next(0, NUM_EQUIPOS_X_GRUPO - 1); //Obtiene el indice del equipo1
                string nomEquipo1 = lstGrupoA.Items[indiceEquipo1].ToString();
                lstGrupoA.Items.RemoveAt(indiceEquipo1); //Elimina de la lista el equipo sorteado
                int indiceEquipo2 = rnd.Next(0, lstGrupoA.Items.Count);//Obtiene el indice del equipo2
                string nomEquipo2 = lstGrupoA.Items[indiceEquipo2].ToString();
                lstGrupoA.Items.RemoveAt(indiceEquipo2);

                lstPartidos.Items.Add(nomEquipo1 + " VS " + nomEquipo2+" - GRUPO A");
            }
            else if(rbtGrupoB.Checked)
            {
                Random rnd = new Random();
                int indiceEquipo1 = rnd.Next(0, NUM_EQUIPOS_X_GRUPO - 1); //Obtiene el indice del equipo1
                string nomEquipo1 = lstGrupoB.Items[indiceEquipo1].ToString();
                lstGrupoB.Items.RemoveAt(indiceEquipo1); //Elimina de la lista el equipo sorteado
                int indiceEquipo2 = rnd.Next(0, lstGrupoB.Items.Count);//Obtiene el indice del equipo2
                string nomEquipo2 = lstGrupoB.Items[indiceEquipo2].ToString();
                lstGrupoB.Items.RemoveAt(indiceEquipo2);

                lstPartidos.Items.Add(nomEquipo1 + " VS " + nomEquipo2 + " - GRUPO B");
            }
            else if (rbtGrupoC.Checked)
            {
                Random rnd = new Random();
                int indiceEquipo1 = rnd.Next(0, NUM_EQUIPOS_X_GRUPO - 1); //Obtiene el indice del equipo1
                string nomEquipo1 = lstGrupoC.Items[indiceEquipo1].ToString();
                lstGrupoC.Items.RemoveAt(indiceEquipo1); //Elimina de la lista el equipo sorteado
                int indiceEquipo2 = rnd.Next(0, lstGrupoC.Items.Count);//Obtiene el indice del equipo2
                string nomEquipo2 = lstGrupoC.Items[indiceEquipo2].ToString();
                lstGrupoC.Items.RemoveAt(indiceEquipo2);

                lstPartidos.Items.Add(nomEquipo1 + " VS " + nomEquipo2 + " - GRUPO C");
            }
            else if (rbtGrupoD.Checked)
            {
                Random rnd = new Random();
                int indiceEquipo1 = rnd.Next(0, NUM_EQUIPOS_X_GRUPO - 1); //Obtiene el indice del equipo1
                string nomEquipo1 = lstGrupoD.Items[indiceEquipo1].ToString();
                lstGrupoD.Items.RemoveAt(indiceEquipo1); //Elimina de la lista el equipo sorteado
                int indiceEquipo2 = rnd.Next(0, lstGrupoD.Items.Count);//Obtiene el indice del equipo2
                string nomEquipo2 = lstGrupoD.Items[indiceEquipo2].ToString();
                lstGrupoD.Items.RemoveAt(indiceEquipo2);

                lstPartidos.Items.Add(nomEquipo1 +  " VS " + nomEquipo2 + " - GRUPO D");
            }

        }
        }
    }


﻿namespace _07SorteoFIFA
{
    partial class frmSorteoFIFA
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtGrupoD = new System.Windows.Forms.RadioButton();
            this.rbtGrupoC = new System.Windows.Forms.RadioButton();
            this.rbtGrupoB = new System.Windows.Forms.RadioButton();
            this.rbtGrupoA = new System.Windows.Forms.RadioButton();
            this.txtEquipo = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnIniciarSorteo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstGrupoD = new System.Windows.Forms.ListBox();
            this.lstGrupoC = new System.Windows.Forms.ListBox();
            this.lstGrupoB = new System.Windows.Forms.ListBox();
            this.lstGrupoA = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstPartidos = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtGrupoD);
            this.groupBox1.Controls.Add(this.rbtGrupoC);
            this.groupBox1.Controls.Add(this.rbtGrupoB);
            this.groupBox1.Controls.Add(this.rbtGrupoA);
            this.groupBox1.Location = new System.Drawing.Point(12, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(138, 132);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione grupo";
            // 
            // rbtGrupoD
            // 
            this.rbtGrupoD.AutoSize = true;
            this.rbtGrupoD.Location = new System.Drawing.Point(7, 94);
            this.rbtGrupoD.Name = "rbtGrupoD";
            this.rbtGrupoD.Size = new System.Drawing.Size(65, 17);
            this.rbtGrupoD.TabIndex = 3;
            this.rbtGrupoD.Text = "Grupo D";
            this.rbtGrupoD.UseVisualStyleBackColor = true;
            this.rbtGrupoD.CheckedChanged += new System.EventHandler(this.rbtGrupoD_CheckedChanged);
            // 
            // rbtGrupoC
            // 
            this.rbtGrupoC.AutoSize = true;
            this.rbtGrupoC.Location = new System.Drawing.Point(7, 70);
            this.rbtGrupoC.Name = "rbtGrupoC";
            this.rbtGrupoC.Size = new System.Drawing.Size(64, 17);
            this.rbtGrupoC.TabIndex = 2;
            this.rbtGrupoC.Text = "Grupo C";
            this.rbtGrupoC.UseVisualStyleBackColor = true;
            this.rbtGrupoC.CheckedChanged += new System.EventHandler(this.rbtGrupoC_CheckedChanged);
            // 
            // rbtGrupoB
            // 
            this.rbtGrupoB.AutoSize = true;
            this.rbtGrupoB.Location = new System.Drawing.Point(7, 44);
            this.rbtGrupoB.Name = "rbtGrupoB";
            this.rbtGrupoB.Size = new System.Drawing.Size(64, 17);
            this.rbtGrupoB.TabIndex = 1;
            this.rbtGrupoB.Text = "Grupo B";
            this.rbtGrupoB.UseVisualStyleBackColor = true;
            this.rbtGrupoB.CheckedChanged += new System.EventHandler(this.rbtGrupoB_CheckedChanged);
            // 
            // rbtGrupoA
            // 
            this.rbtGrupoA.AutoSize = true;
            this.rbtGrupoA.Location = new System.Drawing.Point(7, 20);
            this.rbtGrupoA.Name = "rbtGrupoA";
            this.rbtGrupoA.Size = new System.Drawing.Size(64, 17);
            this.rbtGrupoA.TabIndex = 0;
            this.rbtGrupoA.Text = "Grupo A";
            this.rbtGrupoA.UseVisualStyleBackColor = true;
            this.rbtGrupoA.CheckedChanged += new System.EventHandler(this.rbtGrupoA_CheckedChanged);
            // 
            // txtEquipo
            // 
            this.txtEquipo.Enabled = false;
            this.txtEquipo.Location = new System.Drawing.Point(171, 48);
            this.txtEquipo.Name = "txtEquipo";
            this.txtEquipo.Size = new System.Drawing.Size(123, 20);
            this.txtEquipo.TabIndex = 1;
            this.txtEquipo.TextChanged += new System.EventHandler(this.txtEquipo_TextChanged);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(317, 46);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(84, 23);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnIniciarSorteo
            // 
            this.btnIniciarSorteo.Location = new System.Drawing.Point(239, 123);
            this.btnIniciarSorteo.Name = "btnIniciarSorteo";
            this.btnIniciarSorteo.Size = new System.Drawing.Size(110, 23);
            this.btnIniciarSorteo.TabIndex = 3;
            this.btnIniciarSorteo.Text = "Iniciar Sorteo";
            this.btnIniciarSorteo.UseVisualStyleBackColor = true;
            this.btnIniciarSorteo.Click += new System.EventHandler(this.btnIniciarSorteo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstGrupoD);
            this.groupBox2.Controls.Add(this.lstGrupoC);
            this.groupBox2.Controls.Add(this.lstGrupoB);
            this.groupBox2.Controls.Add(this.lstGrupoA);
            this.groupBox2.Location = new System.Drawing.Point(15, 197);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(386, 212);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Partidos";
            // 
            // lstGrupoD
            // 
            this.lstGrupoD.FormattingEnabled = true;
            this.lstGrupoD.Location = new System.Drawing.Point(285, 29);
            this.lstGrupoD.Name = "lstGrupoD";
            this.lstGrupoD.ScrollAlwaysVisible = true;
            this.lstGrupoD.Size = new System.Drawing.Size(84, 160);
            this.lstGrupoD.TabIndex = 6;
            // 
            // lstGrupoC
            // 
            this.lstGrupoC.FormattingEnabled = true;
            this.lstGrupoC.Location = new System.Drawing.Point(195, 29);
            this.lstGrupoC.Name = "lstGrupoC";
            this.lstGrupoC.ScrollAlwaysVisible = true;
            this.lstGrupoC.Size = new System.Drawing.Size(84, 160);
            this.lstGrupoC.TabIndex = 6;
            // 
            // lstGrupoB
            // 
            this.lstGrupoB.FormattingEnabled = true;
            this.lstGrupoB.Location = new System.Drawing.Point(105, 29);
            this.lstGrupoB.Name = "lstGrupoB";
            this.lstGrupoB.ScrollAlwaysVisible = true;
            this.lstGrupoB.Size = new System.Drawing.Size(84, 160);
            this.lstGrupoB.TabIndex = 1;
            // 
            // lstGrupoA
            // 
            this.lstGrupoA.FormattingEnabled = true;
            this.lstGrupoA.Location = new System.Drawing.Point(15, 29);
            this.lstGrupoA.Name = "lstGrupoA";
            this.lstGrupoA.ScrollAlwaysVisible = true;
            this.lstGrupoA.Size = new System.Drawing.Size(84, 160);
            this.lstGrupoA.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstPartidos);
            this.groupBox3.Location = new System.Drawing.Point(434, 45);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 364);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Partidos";
            // 
            // lstPartidos
            // 
            this.lstPartidos.FormattingEnabled = true;
            this.lstPartidos.Location = new System.Drawing.Point(6, 21);
            this.lstPartidos.Name = "lstPartidos";
            this.lstPartidos.Size = new System.Drawing.Size(188, 329);
            this.lstPartidos.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ingrese Equipos:";
            // 
            // frmSorteoFIFA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnIniciarSorteo);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtEquipo);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmSorteoFIFA";
            this.Text = "Sorteo FIFA 2018";
            this.Load += new System.EventHandler(this.frmSorteoFIFA_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtEquipo;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnIniciarSorteo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lstPartidos;
        private System.Windows.Forms.RadioButton rbtGrupoD;
        private System.Windows.Forms.RadioButton rbtGrupoC;
        private System.Windows.Forms.RadioButton rbtGrupoB;
        private System.Windows.Forms.RadioButton rbtGrupoA;
        private System.Windows.Forms.ListBox lstGrupoD;
        private System.Windows.Forms.ListBox lstGrupoC;
        private System.Windows.Forms.ListBox lstGrupoB;
        private System.Windows.Forms.ListBox lstGrupoA;
        private System.Windows.Forms.Label label1;
    }
}


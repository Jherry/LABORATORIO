﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio9
{
    class Program
    {
        static void titulo()
        {
            Console.Write("Bienvenido: a \"SENATI\"\n");
            Console.WriteLine("===========================");
        }
        static void Main(string[] args)
        {
            //Calcula el costo de una llamada
            const double PRECIOXMINUTO = 0.256325;
            titulo();

            Console.Write("ingrese cantidad de minutos de llamada\n");
            int cantMinutos = int.Parse(Console.ReadLine());
            double precioAPagar = PRECIOXMINUTO * cantMinutos;
            Console.WriteLine($"Usted pagará por {cantMinutos} minutos un total de: {precioAPagar.ToString("c2")}");
            Console.ReadKey();
        }
    }
}

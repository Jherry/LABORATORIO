﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19Pokemones
{
    class Program
    { 
            
            static void Main(string[] args)
            {
                //batalla pokemon
                //string pokemon1="charmander";
                string pokemon2 = "pikachu";
                string pokemon3 = "bolbasor";

            //1.Crear los pokemones
            string[] pokemon = { "charmander", "pikachu", "bolbasor" };
            int[] ataque = {200,300,100};
            int[] defensa = {300,200,500};
            int[] vida = {1000,1000,1000 };

            Console.WriteLine("POKEMONES DISPONIBLES");
            Console.WriteLine("=====================");
            Console.WriteLine($"pokemon1:{pokemon[0]};ataque {ataque[0]};defensa{defensa[0]}; vida:{vida[0]} ");
            Console.WriteLine($"pokemon2:{pokemon[1]};ataque {ataque[1]};defensa{defensa[1]}; vida:{vida[1]} ");
            Console.WriteLine($"pokemon3:{pokemon[2]};ataque {ataque[2]};defensa{defensa[2]}; vida:{vida[2]} ");

            Console.WriteLine("Elija un pokemon de la lista (0-2):");
            int opcion = int.Parse(Console.ReadLine());


            if (opcion<0 || opcion>2)
            {
                Console.WriteLine("ERROR,vuelva a intentar..");
            }
            else
            {
                Random rnd = new Random();
                int opcionSistema = rnd.Next(0, 2);

                Console.Clear();
                Console.WriteLine("INICIA LA BATALLA");
                Console.WriteLine($"{pokemon[opcion]} VS {pokemon[opcionSistema]}");
                Console.WriteLine($"INICIA {pokemon[opcion]}");
                Console.WriteLine("*************************************");
                Console.WriteLine("Presione ENTER para iniciar...");
                Console.ReadKey();
                Console.WriteLine($"(USUARIO) Nombre:{pokemon[opcion]};\nataque {ataque[opcion]};\ndefensa{defensa[opcion]}; \nvida:{vida[opcion]} ");
                Console.WriteLine($"\n(SISTEMA) Nombre:{pokemon[opcionSistema]};\nataque {ataque[opcionSistema]};\ndefensa{defensa[opcionSistema]}; \nvida:{vida[opcionSistema]} ");

                vida[opcionSistema] = vida[opcionSistema] - ataque[opcion] + defensa[opcionSistema];



            }

            Console.ReadKey();



        
        
        }
    }
}

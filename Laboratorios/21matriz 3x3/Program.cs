﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21matriz_3x3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3x3
            int[,] matriz = new int[3,3];//MATRIZ DE DOS DIMENSIONES
            for (int fila = 0; fila < 3; fila++)
            {
                for(int columna = 0; columna < 3; columna++)
                {
                    Console.WriteLine($"Ingrese valor en la posicion ({fila},{columna}):");
                    matriz[fila, columna] = int.Parse(Console.ReadLine());
                }
            }
            
            Console.Clear();
            //recorrer la matriz
            Console.WriteLine($"Aqui la matriz 3x3");
            for (int fila = 0; fila < 3; fila++)
            {
                for (int columna = 0; columna < 3; columna++)
                {
                    Console.Write($"\t{matriz[fila, columna]}");
                }
                Console.WriteLine("\n");
            }
            Console.ReadKey();

        }              
    }
}

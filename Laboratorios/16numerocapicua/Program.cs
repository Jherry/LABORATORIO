﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16numerocapicua
{
    class Program
    {
        static void Main(string[] args)
        {
            //Verifica números de 3 digitos si es o no capicua
            Console.Write("Ingrese un número de 3 cifras: ");
            int numero = int.Parse(Console.ReadLine());
            int numeroOriginal = numero;
            if (numero < 100 || numero > 999)
            {
                Console.WriteLine("ERROR de ingreso, solo se permite 3 cifras.");
            }
            else
            {
                //Unidades, decenas, centenas
                int u, d, c;
                u = numero % 10; //Unidades  423/10 --> cociente=42 y resto=3
                numero = numero / 10; //Cociente (42)
                d = numero % 10; //Decenas  42/10 --> cociente=4 y resto=3
                c = numero / 10; //Cociente

                int numeroInvertido = u * 100 + d * 10 + c;

                if (numeroOriginal == numeroInvertido)
                {
                    Console.WriteLine("Es CAPICUA");
                }
                else
                {
                    Console.WriteLine("NO es CAPICUA");
                }
            }
            Console.ReadKey();


        }
    }
    }


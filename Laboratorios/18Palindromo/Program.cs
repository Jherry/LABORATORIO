﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18Palindromo
{
    class Program
    {
        static string leerPalabra()
        {
            Console.Write("Ingrese una palabra: ");
            return Console.ReadLine();
        }

        static string invertirPalabra(string parametro)
        {
            string resultado = String.Empty;
            for (int i = parametro.Length - 1; i >= 0; i--)
            {
                resultado = String.Concat(resultado, parametro[i]);
            }
            return resultado;
        }

        static void imprimirLetras(string parametro)
        {
            for (int i = 0; i < parametro.Length; i++)
            {
                Console.Write($"{parametro[i]} - ");
            }
        }

        static string limpiarEspacios(string parametro)
        {
            string resultado = String.Empty;

            for (int i = 0; i < parametro.Length; i++)
            {
                char c = parametro[i];
                if (!c.Equals(' '))
                {
                    resultado = String.Concat(resultado, parametro[i]);
                }
            }
            return resultado;
        }

        static void evaluarPalindromo(string palabraOriginal, string palabraInvertida)
        {
            if (limpiarEspacios(palabraOriginal).Equals(limpiarEspacios(palabraInvertida)))
            {
                Console.WriteLine("SI es Palindroma.");
            }
            else
            {
                Console.WriteLine("NO es Palindroma.");
            }
        }

        static void contarVocales(string palabra)
        {
            int cantA = 0, cantE = 0, cantI = 0, cantO = 0, cantU = 0;
            for (int i = 0; i < palabra.Length; i++)
            {
                char c = palabra[i];
                switch (c)
                {
                    case 'a':
                        cantA = cantA + 1;
                        break;
                    case 'e':
                        cantE = cantE + 1;
                        break;
                    case 'i':
                        cantI = cantI + 1;
                        break;
                    case 'o':
                        cantO = cantO + 1;
                        break;
                    case 'u':
                        cantU = cantU + 1;
                        break;
                    default:
                        //Console.WriteLine("Ninguno");
                        break;
                }
            }

            Console.WriteLine($"Número de A:{cantA}");
            Console.WriteLine($"Número de E:{cantE}");
            Console.WriteLine($"Número de I:{cantI}");
            Console.WriteLine($"Número de O:{cantO}");
            Console.WriteLine($"Número de U:{cantU}");
        }

        static int contarPalabras(string frase)
        {
            int cantPalabras = 0;
            for (int i = 0; i < frase.Length; i++)
            {
                char c = frase[i];
                if (c.Equals(' '))
                {
                    cantPalabras++;
                }
            }
            return ++cantPalabras;
        }

        static string[] separarEnPalabras(string frase)
        {
            string[] split = frase.Split(new Char[] { ' ', ',', '.', ':', '\t' });
            return split;
        }

        static string convertirAPalabraCapital(string palabra)
        {
            string resultado = String.Empty;
            for (int i = 0; i < palabra.Length; i++)
            {
                if (i == 0)
                {
                    resultado = String.Concat(resultado, Char.ToUpper(palabra[i]));
                }
                else
                {
                    resultado = String.Concat(resultado, palabra[i]);
                }
            }
            return resultado;
        }

        static void imprimirPalabrasCapitales(string[] parametro)
        {
            foreach (string s in parametro)
            {
                Console.Write($"{ convertirAPalabraCapital(s)} ");
            }
        }

        static void imprimirMenu()
        {
            Console.WriteLine("Programas desarrollados");
            Console.WriteLine("1. PALINDROMO");
            Console.WriteLine("2. INVERTIR PALABRA");
            Console.WriteLine("3. CANTIDAD DE VOCALES");
            Console.WriteLine("4. PRIMERAS LETRAS EN MAYUSCULA");
        }

        static int leerOpcion()
        {
            Console.Write("Elija una opcion(1-4): ");
            return int.Parse(Console.ReadLine());
        }

        static void Main(string[] args)
        {
            imprimirMenu();
            int opcion = leerOpcion();

            string frasePalabra = leerPalabra();

            switch (opcion)
            {
                case 1:
                    string palabraOriginal = frasePalabra;
                    string palabraInvertida = invertirPalabra(palabraOriginal);
                    evaluarPalindromo(palabraOriginal, palabraInvertida);
                    break;
                case 2:
                    Console.WriteLine($"La palabra invertida es: {invertirPalabra(frasePalabra)}");
                    break;
                case 3:
                    contarVocales(frasePalabra.ToLower());
                    break;
                case 4:
                    string[] palabras = separarEnPalabras(frasePalabra);
                    imprimirPalabrasCapitales(palabras);
                    break;
                default:
                    Console.WriteLine("ERROR de ingreso...");
                    break;
            }


            Console.ReadKey();
        }
    }
}

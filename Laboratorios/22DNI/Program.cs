﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22DNI
{
    class Program
    {
        static void Main(string[] args)
        {
            //Forma rucs a partir del DNI
            Console.WriteLine("Ingrese DNI: ");
            string dni = Console.ReadLine();
            string ruc = string.Empty;

            Console.WriteLine("Procedera a formar su RUC ...");
            Console.WriteLine("Es una persona (1)natural o (2)juridica?");

            int opcion = int.Parse(Console.ReadLine());

            switch(opcion)
            {
                case 1:
                    Console.Write("Ingrese un digito de sufijo para su RUC:");
                    int sufijo = int.Parse(Console.ReadLine());
                    ruc=string.Concat("10", dni, sufijo);
                    break;
                case 2:
                    Console.Write("Ingrese un digito de sufijo para su RUC:");
                    int sufijo2= int.Parse(Console.ReadLine());
                    ruc=string.Concat("20", dni, sufijo2);
                    break;
                default:
                    Console.WriteLine("ERROR de opcion...");
                    break;
            }
            Console.WriteLine($"Su RUC es:{ruc}");
            Console.ReadKey();
        }
    }
}

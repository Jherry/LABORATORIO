﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20Arraysimprimir
{
    class Program
    {
        static void Main(string[] args)
        {
            //imprime nombres desde un array
            int i=0;
            string[] nombres= new string[5];
            string valor = string.Empty;
            do
            {
                Console.Write($"Ingrese el Nombre({i})");
                valor = Console.ReadLine();
                nombres[i] = valor;
                i++;
            } while (valor!="$");

            //Console.Clear();
            //recorrer el array
            Console.WriteLine("Inicio de recorrido del Array");
            foreach(string elemento in nombres)
            {
                Console.WriteLine($"{elemento}");
            }
            Console.ReadKey();
        }
    }
}

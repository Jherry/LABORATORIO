﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17Graficos
{
    class Program
    {
        static void menu()
        {
            Console.WriteLine("1. Triangulo Rectangulo");
            Console.WriteLine("2. Triangulo Equilatero");
            Console.WriteLine("3. Cuadrado");
            Console.Write(">>Ingrese opcion(1-3):");
        }

        static void dibuja01()
        {
            //Triangulo de lado n
            Console.Write("Ingrese n: ");
            int n = int.Parse(Console.ReadLine());
            for (int f = 1; f <= n; f++)
            {
                for (int c = 1; c <= f; c++)
                {
                    Console.Write(" * ");
                }
                Console.Write("\n");
            }
        }

        static void dibuja02()
        {
            //Triangulo de lado n
            Console.Write("Ingrese n: ");
            int n = int.Parse(Console.ReadLine());
            if (n % 2 != 0)
            {
                for (int f = 1; f <= n; f++)
                {
                    for (int c = 1; c <= f; c++)
                    {
                        Console.Write(" * ");
                    }
                    Console.Write("\n");
                }
            }
            else
            {
                Console.Write("Este dibujo solo permite numeros impares");
            }
        }

        static void dibuja03()
        {
            //Triangulo de lado n
            Console.Write("Ingrese n: ");
            int n = int.Parse(Console.ReadLine());
            for (int f = 1; f <= n; f++)
            {
                for (int c = 1; c <= n; c++)
                {
                    Console.Write(" * ");
                }
                Console.Write("\n");
            }
        }
        static void Main(string[] args)
        {
            menu();
            int opcion = int.Parse(Console.ReadLine());

            switch (opcion)
            {
                case 1:
                    dibuja01();
                    break;
                case 2:
                    dibuja02();
                    break;
                case 3:
                    dibuja03();
                    break;
                default:
                    Console.WriteLine("ERROR...");
                    break;

            }

            Console.ReadKey();

        }
    }
}

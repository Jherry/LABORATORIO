﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23Ordenarnumerosconarray
{
    class Program
    {
        static int mayor;
        static int menor;
        static int suma;
        static void Main(string[] args)
        {
            
            int[] numeros = new int[6];
            for(int i=1; i<6;i++)
            {
                Console.WriteLine($"Ingrese num({i}): ");
                numeros[i] = int.Parse(Console.ReadLine());
            }
            //realizar operaciones


            for (int i = 1; i < 6; i++)
            {
                suma += numeros[i];
                if(i==1)
                {
                    mayor = numeros[i];
                    menor = numeros[i];
                }
                else
                {
                    if(numeros[i]<menor)
                    {
                        menor = numeros[i];
                    }
                    if (numeros[i]>mayor)
                    {
                        mayor = numeros[i];
                    }

                }
            }
            Console.WriteLine($"El mayor es:{mayor}");
            Console.WriteLine($"El menor es:{menor}");
            Console.WriteLine($"La suma de numeros es:{suma}");
            Console.WriteLine($"El promedio del mayor con el menor es:{suma/5.0}");

            Console.ReadKey();
        }
    }
}

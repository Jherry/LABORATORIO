﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11ViajeSenati
{
    class Program
    {
        static void Main(string[] args)
        {
            //Viajede alumnos de SENATI
            Console.WriteLine("Ingrese cantidad de alumnos a viajar:");
            int cantAlumnos = int.Parse(Console.ReadLine());
            double montoTotal = 0;//inicializar una variable

            /*>100-->s/65.00
             * 50 - 99 -->s/70.00
             * 30 - 49 -->s/95.00
             * <30 -->s/4000.00
             */

            if(cantAlumnos>100 )
            {
                montoTotal = cantAlumnos * 65;
            }
            else if(cantAlumnos>50 && cantAlumnos<99)
            {
                montoTotal = cantAlumnos * 70;
            }
            else if (cantAlumnos>=30 && cantAlumnos<49)
            {
                montoTotal = cantAlumnos * 95;
            }
            else
            {
                montoTotal = 4000;
            }
            
            Console.WriteLine($"El monto total a pagar al BUS es:{montoTotal.ToString("c2")} nuevos soles");
            Console.WriteLine($"el monto a pagar por caga alumno es:{(montoTotal/cantAlumnos).ToString("c2")} nuevos soles");

            Console.ReadKey();
             
        }
    }
}

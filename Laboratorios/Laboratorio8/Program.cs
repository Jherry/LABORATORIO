﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio8
{
    class Program
    {
        static int calculaEdad(int anioNacimiento)
        {
            DateTime fechaActual = DateTime.Now; //obtiene la fecha de la computadora
            int anioActual = fechaActual.Year; //obtiene el año actual
            int mesActual = fechaActual.Month;//obtiene mes actual
            int diaActual = fechaActual.Day;//obtiene dia actual

            int edadA = anioActual - anioNacimiento;//obtiene edad
            return edadA;
        }
        static void titulo()
        {
            Console.Write("Bienvenido: a \"SENATI\"\n");
            Console.WriteLine("===========================");

        }
        static void Main(string[] args)
        {
            //Programa que calcula las edades de personas en una entrevista
            titulo();

            Console.Write("Ingrese su Nombre:");
            string nombre = Console.ReadLine();

            Console.Write("Ingrese año de nacimiento: ");
            int anioNacimiento = int.Parse(Console.ReadLine());

            int edad = calculaEdad(anioNacimiento);

            
            

            Console.WriteLine($"Bienvenido{nombre},usted tiene{edad} para la entrevista");
            

            Console.ReadKey();
        }
    }
}

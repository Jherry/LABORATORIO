﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10PiedraPapelTijera
{
    class Program
    {
        static void menu()
        {
            Console.WriteLine("1. Piedra");
            Console.WriteLine("2. Papel");
            Console.WriteLine("3. Tijera");
            Console.Write(">>Elija una opcion (0-3): ");
        }

        static string leerOpcion(int opcion)
        {
            string opcionUsuario = string.Empty;
            if (opcion == 1)
            {
                opcionUsuario = "Piedra";
            }
            else if (opcion == 2)
            {
                opcionUsuario = "Papel";
            }
            else
            {
                opcionUsuario = "Tijera";
            }

            return opcionUsuario;
        }
        //Metodo que opcione una opcion aleatoria para la PC
        static string opcionPC()
        {
            Random rnd = new Random();
            int opcion = rnd.Next(1, 3);
            return leerOpcion(opcion);
        }

        //Metodo  Iniciar Juego 
        static void iniciarJuego(string opcionUsuario, string opcionSistema)
        {
            
            if (opcionUsuario.Equals("Piedra")) //Usuario:Piedra
            {
                if (opcionSistema.Equals("Piedra"))
                {
                    Console.WriteLine("EMPATE.");
                }
                else if (opcionSistema.Equals("Papel"))
                {
                    Console.WriteLine("SISTEMA GANADOR.");
                }
                else
                {
                    Console.WriteLine("USUARIO GANADOR.");
                }

            }
            else if (opcionUsuario.Equals("Tijera")) //Usuario:Tijera
            {
                if (opcionSistema.Equals("Piedra"))
                {
                    Console.WriteLine("SISTEMA GANADOR.");
                }
                else if (opcionSistema.Equals("Papel"))
                {
                    Console.WriteLine("USUARIO GANADOR.");
                }
                else
                {
                    Console.WriteLine("EMPATE.");
                }
            }
            else //Usuario:papel
            {
               if (opcionSistema.Equals("Piedra")) 
                    {
                        Console.WriteLine("USUARIO GANADOR.");
                    }
                    else if (opcionSistema.Equals("Papel"))
                    {
                        Console.WriteLine("EMPATE.");
                    }
                    
                }
            
            }
        static void Main(string[] args)
        {
            //Juego de piedra papel Tijera
            menu();
            int opcion = int.Parse(Console.ReadLine());

            if (opcion < 1 || opcion > 3)
            {
                Console.WriteLine("Opcion incorrecta, vuelva a intentar...");
            }
            else
            {
                string opcionusuario = leerOpcion(opcion);
                string opcionsistema = opcionPC();

                iniciarJuego(opcionusuario, opcionsistema);
                Console.WriteLine($"Su opción es: {leerOpcion(opcion)}");
                Console.WriteLine($"Opcion de la PC es:{opcionPC()}");
            }



            Console.ReadKey();





        }
    }
}

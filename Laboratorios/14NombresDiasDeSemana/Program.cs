﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14NombresDiasDeSemana
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ingrese dia de semana: ");
            int dia = int.Parse(Console.ReadLine());

            switch (dia)
            {
                case 0:
                    Console.WriteLine("DOMINGO");
                    break;
                case 1:
                    Console.WriteLine("LUNES");
                    break;
                case 2:
                    Console.WriteLine("MARTES");
                    break;
                case 3:
                    Console.WriteLine("MIERCOLES");
                    break;
                case 4:
                    Console.WriteLine("JUEVES");
                    break;
                case 5:
                    Console.WriteLine("VIERNES");
                    break;
                case 6:
                    Console.WriteLine("SABADO");
                    break;
                default:
                    Console.WriteLine("ERROR de ingreso...");
                    break;
            }
            Console.ReadKey();
            }
        }
    }


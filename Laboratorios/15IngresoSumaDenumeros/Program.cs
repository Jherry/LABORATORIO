﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15IngresoSumaDenumeros
{
    class Program
    {
        static void Main(string[] args)
        {
            //suma de n numeros ingresados por teclado
            int i = 1;
            int numero = 0;
            do
            {
                Console.Write($"Ingrese numero({i})");
                numero = int.Parse(Console.ReadLine());
                i++; //contador
            } while (numero > 0);

            Console.WriteLine($"Usted ingreso {i}numeros.");
            Console.ReadKey();
        }
    }
}

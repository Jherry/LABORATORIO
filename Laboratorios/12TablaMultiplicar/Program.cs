﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12TablaMultiplicar
{
    class Program
    {
        static void mostrarMenu()
        {
            Console.WriteLine("TABLA DE MULTIPLICAR");
        Console.WriteLine("1.Con WHILE");
        Console.WriteLine("2.Con DO-WHILE");
        Console.WriteLine("3.Con FOR");
        Console.WriteLine(">>seleccione una opcion (1-3):");
    }
        static void metodowhile(int n)
        {
            Console.WriteLine("Soy while: Aqui la tabla del {n}");
            int i = 1; //Contador
            while (i<=12)
            {
                Console.WriteLine($"\t {i} x {n} = {i * n}");
                i++; //i=i+1 | i+=1
            }
        }
        static void metododowhile(int n)
        {
            Console.WriteLine($"Soy do-while: Aqui la tabla del {n}");
            int i = 1;//contador
            do
            {
                Console.WriteLine($"\t {i} x {n} = {i * n}");
                i++;
            } while (i <= 12);

        }
        static void metodofor(int n)
        {
            Console.WriteLine($"Soy for:Aqui la tabla del {n}");
            for (int i=1; i<=12; i++)
            {
                Console.WriteLine($"{i} x {n}={i*n}");

            }
        }
        static void Main(string[] args)
        {
            //Muestra la tabla de multiplicar de un numero ingresado




            mostrarMenu();

            int opcion = int.Parse(Console.ReadLine());

            if (opcion < 0 || opcion > 3)
            {
                Console.WriteLine("Opcion INCORRECTA!!!...");
            }
            else
            {
                Console.Write($"Usted selecciono la opcion {opcion} \n");
                Console.Write("¿Que tabla de multiplicar generara?:");
            int n = int.Parse(Console.ReadLine());

            //estructura switch
            switch (opcion)
            {
                    case 1:
                     metodowhile(n);
                       break;
                    case 2:
                        metododowhile(n);
                        break;
                    case 3:
                        metodofor(n);
                        break;
        }
                
            }
            Console.ReadKey();
        }
    }
}

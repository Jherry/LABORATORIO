﻿namespace _02CasoIncidencias.Formularios
{
    partial class FrmAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtFAlumnos = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtMAlumnos = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEdadAlumnos = new System.Windows.Forms.TextBox();
            this.txtCodAlumnos = new System.Windows.Forms.TextBox();
            this.txtDNIAlumnos = new System.Windows.Forms.TextBox();
            this.txtapepatAlumnos = new System.Windows.Forms.TextBox();
            this.txtNombreAlumnos = new System.Windows.Forms.TextBox();
            this.txtapematAlumnos = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtYearAlumno = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCursomatrAlumno = new System.Windows.Forms.TextBox();
            this.cboSemeAlum = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNotaPracAlum = new System.Windows.Forms.TextBox();
            this.txtExamenParAlum = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNotaActAlum = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPromedioFinAlum = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnRegistrarAlum = new System.Windows.Forms.Button();
            this.btnCancelarAlum = new System.Windows.Forms.Button();
            this.btnSalirAlumno = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtYearAlumno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbtFAlumnos);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbtMAlumnos);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtEdadAlumnos);
            this.groupBox1.Controls.Add(this.txtCodAlumnos);
            this.groupBox1.Controls.Add(this.txtDNIAlumnos);
            this.groupBox1.Controls.Add(this.txtapepatAlumnos);
            this.groupBox1.Controls.Add(this.txtNombreAlumnos);
            this.groupBox1.Controls.Add(this.txtapematAlumnos);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(26, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 258);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Materno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo (ID)";
            // 
            // rbtFAlumnos
            // 
            this.rbtFAlumnos.AutoSize = true;
            this.rbtFAlumnos.Location = new System.Drawing.Point(143, 167);
            this.rbtFAlumnos.Name = "rbtFAlumnos";
            this.rbtFAlumnos.Size = new System.Drawing.Size(31, 17);
            this.rbtFAlumnos.TabIndex = 17;
            this.rbtFAlumnos.Text = "F";
            this.rbtFAlumnos.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Paterno:";
            // 
            // rbtMAlumnos
            // 
            this.rbtMAlumnos.AutoSize = true;
            this.rbtMAlumnos.Checked = true;
            this.rbtMAlumnos.Location = new System.Drawing.Point(103, 168);
            this.rbtMAlumnos.Name = "rbtMAlumnos";
            this.rbtMAlumnos.Size = new System.Drawing.Size(34, 17);
            this.rbtMAlumnos.TabIndex = 16;
            this.rbtMAlumnos.TabStop = true;
            this.rbtMAlumnos.Text = "M";
            this.rbtMAlumnos.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombres:";
            // 
            // txtEdadAlumnos
            // 
            this.txtEdadAlumnos.Enabled = false;
            this.txtEdadAlumnos.Location = new System.Drawing.Point(103, 218);
            this.txtEdadAlumnos.Name = "txtEdadAlumnos";
            this.txtEdadAlumnos.Size = new System.Drawing.Size(79, 20);
            this.txtEdadAlumnos.TabIndex = 15;
            this.txtEdadAlumnos.TextChanged += new System.EventHandler(this.txtEdadAlumnos_TextChanged);
            // 
            // txtCodAlumnos
            // 
            this.txtCodAlumnos.Enabled = false;
            this.txtCodAlumnos.Location = new System.Drawing.Point(103, 32);
            this.txtCodAlumnos.Name = "txtCodAlumnos";
            this.txtCodAlumnos.Size = new System.Drawing.Size(100, 20);
            this.txtCodAlumnos.TabIndex = 4;
            // 
            // txtDNIAlumnos
            // 
            this.txtDNIAlumnos.Location = new System.Drawing.Point(103, 137);
            this.txtDNIAlumnos.Name = "txtDNIAlumnos";
            this.txtDNIAlumnos.Size = new System.Drawing.Size(100, 20);
            this.txtDNIAlumnos.TabIndex = 12;
            // 
            // txtapepatAlumnos
            // 
            this.txtapepatAlumnos.Location = new System.Drawing.Point(103, 58);
            this.txtapepatAlumnos.Name = "txtapepatAlumnos";
            this.txtapepatAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtapepatAlumnos.TabIndex = 5;
            // 
            // txtNombreAlumnos
            // 
            this.txtNombreAlumnos.Location = new System.Drawing.Point(103, 110);
            this.txtNombreAlumnos.Name = "txtNombreAlumnos";
            this.txtNombreAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtNombreAlumnos.TabIndex = 11;
            // 
            // txtapematAlumnos
            // 
            this.txtapematAlumnos.Location = new System.Drawing.Point(103, 84);
            this.txtapematAlumnos.Name = "txtapematAlumnos";
            this.txtapematAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtapematAlumnos.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Edad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "DNI:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Año Nacimiento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Sexo:";
            // 
            // txtYearAlumno
            // 
            this.txtYearAlumno.Location = new System.Drawing.Point(103, 192);
            this.txtYearAlumno.Name = "txtYearAlumno";
            this.txtYearAlumno.Size = new System.Drawing.Size(169, 20);
            this.txtYearAlumno.TabIndex = 18;
            this.txtYearAlumno.TextChanged += new System.EventHandler(this.txtYearAlumno_TextChanged);
            this.txtYearAlumno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYearAlumno_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboSemeAlum);
            this.groupBox2.Controls.Add(this.txtCursomatrAlumno);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(445, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 100);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Academicos";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Semestre:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Curso Matriculado:";
            // 
            // txtCursomatrAlumno
            // 
            this.txtCursomatrAlumno.Location = new System.Drawing.Point(149, 64);
            this.txtCursomatrAlumno.Name = "txtCursomatrAlumno";
            this.txtCursomatrAlumno.Size = new System.Drawing.Size(150, 20);
            this.txtCursomatrAlumno.TabIndex = 2;
            // 
            // cboSemeAlum
            // 
            this.cboSemeAlum.FormattingEnabled = true;
            this.cboSemeAlum.Location = new System.Drawing.Point(149, 23);
            this.cboSemeAlum.Name = "cboSemeAlum";
            this.cboSemeAlum.Size = new System.Drawing.Size(150, 21);
            this.cboSemeAlum.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtPromedioFinAlum);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtNotaActAlum);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtExamenParAlum);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtNotaPracAlum);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(445, 150);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(343, 141);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Nota Practica:";
            // 
            // txtNotaPracAlum
            // 
            this.txtNotaPracAlum.Location = new System.Drawing.Point(124, 27);
            this.txtNotaPracAlum.Name = "txtNotaPracAlum";
            this.txtNotaPracAlum.Size = new System.Drawing.Size(108, 20);
            this.txtNotaPracAlum.TabIndex = 1;
            // 
            // txtExamenParAlum
            // 
            this.txtExamenParAlum.Location = new System.Drawing.Point(124, 55);
            this.txtExamenParAlum.Name = "txtExamenParAlum";
            this.txtExamenParAlum.Size = new System.Drawing.Size(108, 20);
            this.txtExamenParAlum.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Examen Parcial:";
            // 
            // txtNotaActAlum
            // 
            this.txtNotaActAlum.Location = new System.Drawing.Point(124, 81);
            this.txtNotaActAlum.Name = "txtNotaActAlum";
            this.txtNotaActAlum.Size = new System.Drawing.Size(108, 20);
            this.txtNotaActAlum.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Nota Actitudinal:";
            // 
            // txtPromedioFinAlum
            // 
            this.txtPromedioFinAlum.Location = new System.Drawing.Point(124, 107);
            this.txtPromedioFinAlum.Name = "txtPromedioFinAlum";
            this.txtPromedioFinAlum.Size = new System.Drawing.Size(108, 20);
            this.txtPromedioFinAlum.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 107);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Promedio Final:";
            // 
            // btnRegistrarAlum
            // 
            this.btnRegistrarAlum.Location = new System.Drawing.Point(253, 350);
            this.btnRegistrarAlum.Name = "btnRegistrarAlum";
            this.btnRegistrarAlum.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrarAlum.TabIndex = 23;
            this.btnRegistrarAlum.Text = "Registrar";
            this.btnRegistrarAlum.UseVisualStyleBackColor = true;
            this.btnRegistrarAlum.Click += new System.EventHandler(this.btnRegistrarAlum_Click);
            // 
            // btnCancelarAlum
            // 
            this.btnCancelarAlum.Location = new System.Drawing.Point(380, 350);
            this.btnCancelarAlum.Name = "btnCancelarAlum";
            this.btnCancelarAlum.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarAlum.TabIndex = 24;
            this.btnCancelarAlum.Text = "Cancelar";
            this.btnCancelarAlum.UseVisualStyleBackColor = true;
            // 
            // btnSalirAlumno
            // 
            this.btnSalirAlumno.Location = new System.Drawing.Point(507, 350);
            this.btnSalirAlumno.Name = "btnSalirAlumno";
            this.btnSalirAlumno.Size = new System.Drawing.Size(75, 23);
            this.btnSalirAlumno.TabIndex = 25;
            this.btnSalirAlumno.Text = "Salir";
            this.btnSalirAlumno.UseVisualStyleBackColor = true;
            this.btnSalirAlumno.Click += new System.EventHandler(this.btnSalirAlumno_Click);
            // 
            // FrmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalirAlumno);
            this.Controls.Add(this.btnCancelarAlum);
            this.Controls.Add(this.btnRegistrarAlum);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmAlumnos";
            this.Text = "FrmAlumnos";
            this.Load += new System.EventHandler(this.FrmAlumnos_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmAlumnos_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtFAlumnos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtMAlumnos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEdadAlumnos;
        private System.Windows.Forms.TextBox txtCodAlumnos;
        private System.Windows.Forms.TextBox txtDNIAlumnos;
        private System.Windows.Forms.TextBox txtapepatAlumnos;
        private System.Windows.Forms.TextBox txtNombreAlumnos;
        private System.Windows.Forms.TextBox txtapematAlumnos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtYearAlumno;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboSemeAlum;
        private System.Windows.Forms.TextBox txtCursomatrAlumno;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPromedioFinAlum;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNotaActAlum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtExamenParAlum;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNotaPracAlum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnRegistrarAlum;
        private System.Windows.Forms.Button btnCancelarAlum;
        private System.Windows.Forms.Button btnSalirAlumno;
    }
}
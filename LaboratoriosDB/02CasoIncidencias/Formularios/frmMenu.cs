﻿

using _02CasoIncidencias.Formularios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02CasoIncidencias
{
    public partial class FrmMenuIncidencias : Form
    {
        public FrmMenuIncidencias()
        {
            InitializeComponent();
        }

        private void instructivosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAlumnos frmAlumnos = new FrmAlumnos();
            frmAlumnos.MdiParent = this;
            frmAlumnos.Show();

        }

        private void nuevoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmInstructores frmInstructores = new FrmInstructores();
            frmInstructores.MdiParent = this;
            frmInstructores.Show();
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmIncidencias frmIncidencias = new FrmIncidencias();
            frmIncidencias.MdiParent = this;
            frmIncidencias.Show();
        }
    }
}

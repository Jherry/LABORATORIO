﻿namespace _02CasoIncidencias.Formularios
{
    partial class frmPersonas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodPersona = new System.Windows.Forms.TextBox();
            this.txtapepatPersona = new System.Windows.Forms.TextBox();
            this.txtapematPersona = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombrePersona = new System.Windows.Forms.TextBox();
            this.txtDNIPersona = new System.Windows.Forms.TextBox();
            this.txtEdadPersona = new System.Windows.Forms.TextBox();
            this.rbtMPersona = new System.Windows.Forms.RadioButton();
            this.rbtFPersona = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtYearPersona = new System.Windows.Forms.TextBox();
            this.btnCancelarPersona = new System.Windows.Forms.Button();
            this.btnRegistrarPersona = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo (ID)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Paterno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Materno:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombres:";
            // 
            // txtCodPersona
            // 
            this.txtCodPersona.Location = new System.Drawing.Point(103, 32);
            this.txtCodPersona.Name = "txtCodPersona";
            this.txtCodPersona.Size = new System.Drawing.Size(100, 20);
            this.txtCodPersona.TabIndex = 4;
            // 
            // txtapepatPersona
            // 
            this.txtapepatPersona.Location = new System.Drawing.Point(103, 58);
            this.txtapepatPersona.Name = "txtapepatPersona";
            this.txtapepatPersona.Size = new System.Drawing.Size(226, 20);
            this.txtapepatPersona.TabIndex = 5;
            this.txtapepatPersona.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtapematPersona
            // 
            this.txtapematPersona.Location = new System.Drawing.Point(103, 84);
            this.txtapematPersona.Name = "txtapematPersona";
            this.txtapematPersona.Size = new System.Drawing.Size(226, 20);
            this.txtapematPersona.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "DNI:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Sexo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Año Nacimiento:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Edad:";
            // 
            // txtNombrePersona
            // 
            this.txtNombrePersona.Location = new System.Drawing.Point(103, 110);
            this.txtNombrePersona.Name = "txtNombrePersona";
            this.txtNombrePersona.Size = new System.Drawing.Size(226, 20);
            this.txtNombrePersona.TabIndex = 11;
            // 
            // txtDNIPersona
            // 
            this.txtDNIPersona.Location = new System.Drawing.Point(103, 137);
            this.txtDNIPersona.Name = "txtDNIPersona";
            this.txtDNIPersona.Size = new System.Drawing.Size(100, 20);
            this.txtDNIPersona.TabIndex = 12;
            // 
            // txtEdadPersona
            // 
            this.txtEdadPersona.Location = new System.Drawing.Point(103, 218);
            this.txtEdadPersona.Name = "txtEdadPersona";
            this.txtEdadPersona.Size = new System.Drawing.Size(79, 20);
            this.txtEdadPersona.TabIndex = 15;
            // 
            // rbtMPersona
            // 
            this.rbtMPersona.AutoSize = true;
            this.rbtMPersona.Checked = true;
            this.rbtMPersona.Location = new System.Drawing.Point(103, 168);
            this.rbtMPersona.Name = "rbtMPersona";
            this.rbtMPersona.Size = new System.Drawing.Size(34, 17);
            this.rbtMPersona.TabIndex = 16;
            this.rbtMPersona.TabStop = true;
            this.rbtMPersona.Text = "M";
            this.rbtMPersona.UseVisualStyleBackColor = true;
            // 
            // rbtFPersona
            // 
            this.rbtFPersona.AutoSize = true;
            this.rbtFPersona.Location = new System.Drawing.Point(143, 167);
            this.rbtFPersona.Name = "rbtFPersona";
            this.rbtFPersona.Size = new System.Drawing.Size(31, 17);
            this.rbtFPersona.TabIndex = 17;
            this.rbtFPersona.Text = "F";
            this.rbtFPersona.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtYearPersona);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbtFPersona);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbtMPersona);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtEdadPersona);
            this.groupBox1.Controls.Add(this.txtCodPersona);
            this.groupBox1.Controls.Add(this.txtDNIPersona);
            this.groupBox1.Controls.Add(this.txtapepatPersona);
            this.groupBox1.Controls.Add(this.txtNombrePersona);
            this.groupBox1.Controls.Add(this.txtapematPersona);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(22, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 258);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtYearPersona
            // 
            this.txtYearPersona.Location = new System.Drawing.Point(103, 192);
            this.txtYearPersona.Name = "txtYearPersona";
            this.txtYearPersona.Size = new System.Drawing.Size(188, 20);
            this.txtYearPersona.TabIndex = 18;
            // 
            // btnCancelarPersona
            // 
            this.btnCancelarPersona.Location = new System.Drawing.Point(416, 362);
            this.btnCancelarPersona.Name = "btnCancelarPersona";
            this.btnCancelarPersona.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarPersona.TabIndex = 28;
            this.btnCancelarPersona.Text = "Cancelar";
            this.btnCancelarPersona.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarPersona
            // 
            this.btnRegistrarPersona.Location = new System.Drawing.Point(277, 362);
            this.btnRegistrarPersona.Name = "btnRegistrarPersona";
            this.btnRegistrarPersona.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrarPersona.TabIndex = 27;
            this.btnRegistrarPersona.Text = "Registrar";
            this.btnRegistrarPersona.UseVisualStyleBackColor = true;
            // 
            // frmPersonas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCancelarPersona);
            this.Controls.Add(this.btnRegistrarPersona);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmPersonas";
            this.Text = "Registro de personas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodPersona;
        private System.Windows.Forms.TextBox txtapepatPersona;
        private System.Windows.Forms.TextBox txtapematPersona;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombrePersona;
        private System.Windows.Forms.TextBox txtDNIPersona;
        private System.Windows.Forms.TextBox txtEdadPersona;
        private System.Windows.Forms.RadioButton rbtMPersona;
        private System.Windows.Forms.RadioButton rbtFPersona;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtYearPersona;
        private System.Windows.Forms.Button btnCancelarPersona;
        private System.Windows.Forms.Button btnRegistrarPersona;
    }
}
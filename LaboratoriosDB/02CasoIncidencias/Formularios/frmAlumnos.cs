﻿using _02CasoIncidencias.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02CasoIncidencias.Formularios
{
    public partial class FrmAlumnos : Form
    {
        public FrmAlumnos()
        {
            InitializeComponent();
        }
        public char obtenerSexoAlum()
        {
            char rpta;
            if(rbtMAlumnos.Checked)
            {
                rpta = 'M';
            }
            else
            {
                rpta = 'F';
            }
            return rpta;
        }

        private void btnSalirAlumno_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            //Cargar Semestres

            cboSemeAlum.Items.Add("--Seleccione Opcion--");
            cboSemeAlum.Items.Add("I");
            cboSemeAlum.Items.Add("II");
            cboSemeAlum.Items.Add("III");
            cboSemeAlum.Items.Add("IV");
            cboSemeAlum.Items.Add("V");
            cboSemeAlum.Items.Add("VI");
            cboSemeAlum.SelectedIndex = 0;
        }

        private void txtEdadAlumnos_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtYearAlumno_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void FrmAlumnos_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        
            

        
        }

        private void txtYearAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)13)
            {
                try
                {
                    int yearOld = int.Parse(txtYearAlumno.Text);
                    if(yearOld<1980)
                    {
                        MessageBox.Show($"El año Ingresado no corresponde, intente nuevamente.");
                    }

                    DateTime fechaActual = DateTime.Today;
                    int yearCurrent = fechaActual.Year;


                    int edad = yearCurrent - yearOld;

                    txtEdadAlumnos.Text = edad.ToString();

                }               
                


                catch (Exception ex)
                {

                    MessageBox.Show($"ERROR:{ex.Message}");
                }
                finally
                {
                    txtYearAlumno.Clear();
                    txtYearAlumno.Focus();
                }
            }
        }

        private void btnRegistrarAlum_Click(object sender, EventArgs e)
        {
            
            
            string _apepatAlum = txtapepatAlumnos.Text;
            string _apematAlum = txtapematAlumnos.Text;
            string _nomAlum = txtNombreAlumnos.Text;
            char _sexAlum = obtenerSexoAlum();
            int _yearOld = int.Parse(txtYearAlumno.Text);
            int _edad = int.Parse(txtEdadAlumnos.Text);
            string _semestreAlum = cboSemeAlum.SelectedItem.ToString();
            string _cursoMatAlum = txtCursomatrAlumno.Text;
            double _notaPracticaAlum = double.Parse(txtNotaPracAlum.Text);
            double _notaActitudAlum = double.Parse(txtNotaActAlum.Text);
            double _examenParcial = double.Parse(txtExamenParAlum.Text);

            string _codAlumno = String.Empty;
           // generarCodigo() //Generar el codigo

            Alumno alumno1 = new Alumno(_codAlumno, _apepatAlum, _apematAlum, _nomAlum, _sexAlum, _yearOld, _edad, _semestreAlum, _cursoMatAlum,
                _notaPracticaAlum, _notaActitudAlum, _examenParcial);
        }
    }
}

﻿namespace _02CasoIncidencias.Formularios
{
    partial class FrmIncidencias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtYearAlumno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtFAlumnos = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtMAlumnos = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEdadAlumnos = new System.Windows.Forms.TextBox();
            this.txtCodIncidencia = new System.Windows.Forms.TextBox();
            this.txtDNIAlumnos = new System.Windows.Forms.TextBox();
            this.txtapepatAlumnos = new System.Windows.Forms.TextBox();
            this.txtNombreAlumnos = new System.Windows.Forms.TextBox();
            this.txtapematAlumnos = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDescripcionIncidencia = new System.Windows.Forms.TextBox();
            this.cboSedesIncidencias = new System.Windows.Forms.ComboBox();
            this.lstLaboratoriosIncidencia = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtYearAlumno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbtFAlumnos);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbtMAlumnos);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtEdadAlumnos);
            this.groupBox1.Controls.Add(this.txtCodIncidencia);
            this.groupBox1.Controls.Add(this.txtDNIAlumnos);
            this.groupBox1.Controls.Add(this.txtapepatAlumnos);
            this.groupBox1.Controls.Add(this.txtNombreAlumnos);
            this.groupBox1.Controls.Add(this.txtapematAlumnos);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(28, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 258);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtYearAlumno
            // 
            this.txtYearAlumno.Location = new System.Drawing.Point(103, 192);
            this.txtYearAlumno.Name = "txtYearAlumno";
            this.txtYearAlumno.Size = new System.Drawing.Size(169, 20);
            this.txtYearAlumno.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Materno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo (ID)";
            // 
            // rbtFAlumnos
            // 
            this.rbtFAlumnos.AutoSize = true;
            this.rbtFAlumnos.Location = new System.Drawing.Point(143, 167);
            this.rbtFAlumnos.Name = "rbtFAlumnos";
            this.rbtFAlumnos.Size = new System.Drawing.Size(31, 17);
            this.rbtFAlumnos.TabIndex = 17;
            this.rbtFAlumnos.Text = "F";
            this.rbtFAlumnos.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Paterno:";
            // 
            // rbtMAlumnos
            // 
            this.rbtMAlumnos.AutoSize = true;
            this.rbtMAlumnos.Checked = true;
            this.rbtMAlumnos.Location = new System.Drawing.Point(103, 168);
            this.rbtMAlumnos.Name = "rbtMAlumnos";
            this.rbtMAlumnos.Size = new System.Drawing.Size(34, 17);
            this.rbtMAlumnos.TabIndex = 16;
            this.rbtMAlumnos.TabStop = true;
            this.rbtMAlumnos.Text = "M";
            this.rbtMAlumnos.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombres:";
            // 
            // txtEdadAlumnos
            // 
            this.txtEdadAlumnos.Location = new System.Drawing.Point(103, 218);
            this.txtEdadAlumnos.Name = "txtEdadAlumnos";
            this.txtEdadAlumnos.Size = new System.Drawing.Size(79, 20);
            this.txtEdadAlumnos.TabIndex = 15;
            // 
            // txtCodIncidencia
            // 
            this.txtCodIncidencia.Location = new System.Drawing.Point(103, 32);
            this.txtCodIncidencia.Name = "txtCodIncidencia";
            this.txtCodIncidencia.Size = new System.Drawing.Size(100, 20);
            this.txtCodIncidencia.TabIndex = 4;
            // 
            // txtDNIAlumnos
            // 
            this.txtDNIAlumnos.Location = new System.Drawing.Point(103, 137);
            this.txtDNIAlumnos.Name = "txtDNIAlumnos";
            this.txtDNIAlumnos.Size = new System.Drawing.Size(100, 20);
            this.txtDNIAlumnos.TabIndex = 12;
            // 
            // txtapepatAlumnos
            // 
            this.txtapepatAlumnos.Location = new System.Drawing.Point(103, 58);
            this.txtapepatAlumnos.Name = "txtapepatAlumnos";
            this.txtapepatAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtapepatAlumnos.TabIndex = 5;
            // 
            // txtNombreAlumnos
            // 
            this.txtNombreAlumnos.Location = new System.Drawing.Point(103, 110);
            this.txtNombreAlumnos.Name = "txtNombreAlumnos";
            this.txtNombreAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtNombreAlumnos.TabIndex = 11;
            // 
            // txtapematAlumnos
            // 
            this.txtapematAlumnos.Location = new System.Drawing.Point(103, 84);
            this.txtapematAlumnos.Name = "txtapematAlumnos";
            this.txtapematAlumnos.Size = new System.Drawing.Size(226, 20);
            this.txtapematAlumnos.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Edad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "DNI:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Año Nacimiento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Sexo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lstLaboratoriosIncidencia);
            this.groupBox2.Controls.Add(this.cboSedesIncidencias);
            this.groupBox2.Location = new System.Drawing.Point(429, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(344, 170);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sedes y laboratorios";
            // 
            // txtDescripcionIncidencia
            // 
            this.txtDescripcionIncidencia.Location = new System.Drawing.Point(429, 240);
            this.txtDescripcionIncidencia.Multiline = true;
            this.txtDescripcionIncidencia.Name = "txtDescripcionIncidencia";
            this.txtDescripcionIncidencia.Size = new System.Drawing.Size(344, 58);
            this.txtDescripcionIncidencia.TabIndex = 23;
            // 
            // cboSedesIncidencias
            // 
            this.cboSedesIncidencias.FormattingEnabled = true;
            this.cboSedesIncidencias.Location = new System.Drawing.Point(23, 50);
            this.cboSedesIncidencias.Name = "cboSedesIncidencias";
            this.cboSedesIncidencias.Size = new System.Drawing.Size(121, 21);
            this.cboSedesIncidencias.TabIndex = 0;
            // 
            // lstLaboratoriosIncidencia
            // 
            this.lstLaboratoriosIncidencia.FormattingEnabled = true;
            this.lstLaboratoriosIncidencia.Location = new System.Drawing.Point(169, 48);
            this.lstLaboratoriosIncidencia.Name = "lstLaboratoriosIncidencia";
            this.lstLaboratoriosIncidencia.Size = new System.Drawing.Size(148, 108);
            this.lstLaboratoriosIncidencia.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(169, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Laboratorios";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Sedes";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(429, 221);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Ingrese una breve descripcion:";
            // 
            // FrmIncidencias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtDescripcionIncidencia);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmIncidencias";
            this.Text = "FrmIncidencias";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtYearAlumno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtFAlumnos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtMAlumnos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEdadAlumnos;
        private System.Windows.Forms.TextBox txtCodIncidencia;
        private System.Windows.Forms.TextBox txtDNIAlumnos;
        private System.Windows.Forms.TextBox txtapepatAlumnos;
        private System.Windows.Forms.TextBox txtNombreAlumnos;
        private System.Windows.Forms.TextBox txtapematAlumnos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox lstLaboratoriosIncidencia;
        private System.Windows.Forms.ComboBox cboSedesIncidencias;
        private System.Windows.Forms.TextBox txtDescripcionIncidencia;
        private System.Windows.Forms.Label label11;
    }
}
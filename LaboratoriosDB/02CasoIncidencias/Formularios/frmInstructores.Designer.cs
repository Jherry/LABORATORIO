﻿namespace _02CasoIncidencias.Formularios
{
    partial class FrmInstructores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtYearInstructor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtFInstructor = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtMInstructor = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEdadInstructor = new System.Windows.Forms.TextBox();
            this.txtCodInstructor = new System.Windows.Forms.TextBox();
            this.txtDNIInstructor = new System.Windows.Forms.TextBox();
            this.txtapepatInstructor = new System.Windows.Forms.TextBox();
            this.txtNombreInstructor = new System.Windows.Forms.TextBox();
            this.txtapemaInstructor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancelarInst = new System.Windows.Forms.Button();
            this.btnRegistrarInst = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSueldoInstructor = new System.Windows.Forms.TextBox();
            this.txtEscolaridadInstructor = new System.Windows.Forms.TextBox();
            this.txtSalarioHoraInstrcutor = new System.Windows.Forms.TextBox();
            this.txtHorasSemanaInstructor = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cboContratoInstructor = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboEspecialidadInstructor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSalirInstructor = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtYearInstructor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbtFInstructor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbtMInstructor);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtEdadInstructor);
            this.groupBox1.Controls.Add(this.txtCodInstructor);
            this.groupBox1.Controls.Add(this.txtDNIInstructor);
            this.groupBox1.Controls.Add(this.txtapepatInstructor);
            this.groupBox1.Controls.Add(this.txtNombreInstructor);
            this.groupBox1.Controls.Add(this.txtapemaInstructor);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(27, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 258);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtYearInstructor
            // 
            this.txtYearInstructor.Location = new System.Drawing.Point(103, 192);
            this.txtYearInstructor.Name = "txtYearInstructor";
            this.txtYearInstructor.Size = new System.Drawing.Size(154, 20);
            this.txtYearInstructor.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Materno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo (ID)";
            // 
            // rbtFInstructor
            // 
            this.rbtFInstructor.AutoSize = true;
            this.rbtFInstructor.Location = new System.Drawing.Point(143, 167);
            this.rbtFInstructor.Name = "rbtFInstructor";
            this.rbtFInstructor.Size = new System.Drawing.Size(31, 17);
            this.rbtFInstructor.TabIndex = 17;
            this.rbtFInstructor.Text = "F";
            this.rbtFInstructor.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Paterno:";
            // 
            // rbtMInstructor
            // 
            this.rbtMInstructor.AutoSize = true;
            this.rbtMInstructor.Checked = true;
            this.rbtMInstructor.Location = new System.Drawing.Point(103, 168);
            this.rbtMInstructor.Name = "rbtMInstructor";
            this.rbtMInstructor.Size = new System.Drawing.Size(34, 17);
            this.rbtMInstructor.TabIndex = 16;
            this.rbtMInstructor.TabStop = true;
            this.rbtMInstructor.Text = "M";
            this.rbtMInstructor.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombres:";
            // 
            // txtEdadInstructor
            // 
            this.txtEdadInstructor.Location = new System.Drawing.Point(103, 218);
            this.txtEdadInstructor.Name = "txtEdadInstructor";
            this.txtEdadInstructor.Size = new System.Drawing.Size(79, 20);
            this.txtEdadInstructor.TabIndex = 15;
            // 
            // txtCodInstructor
            // 
            this.txtCodInstructor.Location = new System.Drawing.Point(103, 32);
            this.txtCodInstructor.Name = "txtCodInstructor";
            this.txtCodInstructor.Size = new System.Drawing.Size(100, 20);
            this.txtCodInstructor.TabIndex = 4;
            // 
            // txtDNIInstructor
            // 
            this.txtDNIInstructor.Location = new System.Drawing.Point(103, 137);
            this.txtDNIInstructor.Name = "txtDNIInstructor";
            this.txtDNIInstructor.Size = new System.Drawing.Size(100, 20);
            this.txtDNIInstructor.TabIndex = 12;
            // 
            // txtapepatInstructor
            // 
            this.txtapepatInstructor.Location = new System.Drawing.Point(103, 58);
            this.txtapepatInstructor.Name = "txtapepatInstructor";
            this.txtapepatInstructor.Size = new System.Drawing.Size(226, 20);
            this.txtapepatInstructor.TabIndex = 5;
            // 
            // txtNombreInstructor
            // 
            this.txtNombreInstructor.Location = new System.Drawing.Point(103, 110);
            this.txtNombreInstructor.Name = "txtNombreInstructor";
            this.txtNombreInstructor.Size = new System.Drawing.Size(226, 20);
            this.txtNombreInstructor.TabIndex = 11;
            // 
            // txtapemaInstructor
            // 
            this.txtapemaInstructor.Location = new System.Drawing.Point(103, 84);
            this.txtapemaInstructor.Name = "txtapemaInstructor";
            this.txtapemaInstructor.Size = new System.Drawing.Size(226, 20);
            this.txtapemaInstructor.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Edad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "DNI:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Año Nacimiento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Sexo:";
            // 
            // btnCancelarInst
            // 
            this.btnCancelarInst.Location = new System.Drawing.Point(371, 357);
            this.btnCancelarInst.Name = "btnCancelarInst";
            this.btnCancelarInst.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarInst.TabIndex = 26;
            this.btnCancelarInst.Text = "Cancelar";
            this.btnCancelarInst.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarInst
            // 
            this.btnRegistrarInst.Location = new System.Drawing.Point(232, 357);
            this.btnRegistrarInst.Name = "btnRegistrarInst";
            this.btnRegistrarInst.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrarInst.TabIndex = 25;
            this.btnRegistrarInst.Text = "Registrar";
            this.btnRegistrarInst.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtSueldoInstructor);
            this.groupBox2.Controls.Add(this.txtEscolaridadInstructor);
            this.groupBox2.Controls.Add(this.txtSalarioHoraInstrcutor);
            this.groupBox2.Controls.Add(this.txtHorasSemanaInstructor);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cboContratoInstructor);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cboEspecialidadInstructor);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(442, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 258);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Academicos";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(199, 196);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "S/.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(199, 170);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "S/.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(199, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "S/.";
            // 
            // txtSueldoInstructor
            // 
            this.txtSueldoInstructor.Location = new System.Drawing.Point(130, 193);
            this.txtSueldoInstructor.Name = "txtSueldoInstructor";
            this.txtSueldoInstructor.Size = new System.Drawing.Size(63, 20);
            this.txtSueldoInstructor.TabIndex = 16;
            // 
            // txtEscolaridadInstructor
            // 
            this.txtEscolaridadInstructor.Location = new System.Drawing.Point(130, 168);
            this.txtEscolaridadInstructor.Name = "txtEscolaridadInstructor";
            this.txtEscolaridadInstructor.Size = new System.Drawing.Size(63, 20);
            this.txtEscolaridadInstructor.TabIndex = 15;
            // 
            // txtSalarioHoraInstrcutor
            // 
            this.txtSalarioHoraInstrcutor.Location = new System.Drawing.Point(130, 144);
            this.txtSalarioHoraInstrcutor.Name = "txtSalarioHoraInstrcutor";
            this.txtSalarioHoraInstrcutor.Size = new System.Drawing.Size(63, 20);
            this.txtSalarioHoraInstrcutor.TabIndex = 14;
            // 
            // txtHorasSemanaInstructor
            // 
            this.txtHorasSemanaInstructor.Location = new System.Drawing.Point(130, 119);
            this.txtHorasSemanaInstructor.Name = "txtHorasSemanaInstructor";
            this.txtHorasSemanaInstructor.Size = new System.Drawing.Size(63, 20);
            this.txtHorasSemanaInstructor.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Sueldo Total:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 173);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Escolaridad (8%):";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Salario Hora:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Horas por Semana:";
            // 
            // cboContratoInstructor
            // 
            this.cboContratoInstructor.FormattingEnabled = true;
            this.cboContratoInstructor.Location = new System.Drawing.Point(130, 58);
            this.cboContratoInstructor.Name = "cboContratoInstructor";
            this.cboContratoInstructor.Size = new System.Drawing.Size(121, 21);
            this.cboContratoInstructor.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tipo de Contrato:";
            // 
            // cboEspecialidadInstructor
            // 
            this.cboEspecialidadInstructor.FormattingEnabled = true;
            this.cboEspecialidadInstructor.Location = new System.Drawing.Point(130, 30);
            this.cboEspecialidadInstructor.Name = "cboEspecialidadInstructor";
            this.cboEspecialidadInstructor.Size = new System.Drawing.Size(121, 21);
            this.cboEspecialidadInstructor.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Especialidad:";
            // 
            // btnSalirInstructor
            // 
            this.btnSalirInstructor.Location = new System.Drawing.Point(510, 357);
            this.btnSalirInstructor.Name = "btnSalirInstructor";
            this.btnSalirInstructor.Size = new System.Drawing.Size(75, 23);
            this.btnSalirInstructor.TabIndex = 28;
            this.btnSalirInstructor.Text = "Salir";
            this.btnSalirInstructor.UseVisualStyleBackColor = true;
            this.btnSalirInstructor.Click += new System.EventHandler(this.btnSalirInstructor_Click);
            // 
            // FrmInstructores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalirInstructor);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancelarInst);
            this.Controls.Add(this.btnRegistrarInst);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmInstructores";
            this.Text = "FrmInstructores";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtFInstructor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtMInstructor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEdadInstructor;
        private System.Windows.Forms.TextBox txtCodInstructor;
        private System.Windows.Forms.TextBox txtDNIInstructor;
        private System.Windows.Forms.TextBox txtapepatInstructor;
        private System.Windows.Forms.TextBox txtNombreInstructor;
        private System.Windows.Forms.TextBox txtapemaInstructor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtYearInstructor;
        private System.Windows.Forms.Button btnCancelarInst;
        private System.Windows.Forms.Button btnRegistrarInst;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtSueldoInstructor;
        private System.Windows.Forms.TextBox txtEscolaridadInstructor;
        private System.Windows.Forms.TextBox txtSalarioHoraInstrcutor;
        private System.Windows.Forms.TextBox txtHorasSemanaInstructor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboContratoInstructor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboEspecialidadInstructor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnSalirInstructor;
    }
}
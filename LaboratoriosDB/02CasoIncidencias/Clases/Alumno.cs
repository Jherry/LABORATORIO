﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _02CasoIncidencias.Clases
{
    public class Alumno:Persona
    {
        private string semestreAlum;
        private string cursoMatAlum;
        private double notaPracticaAlum;
        private double notaActitudAlum;
        private double examenParcialAlum;
        private double promFinalAlum;

        public Alumno(string codPersona, string apepatPersona, 
            string apematPersona, string nomPersona, 
            char sexoPersona, string dniPersona, 
            int yearOldPersona, int edadPersona,
            string semestreAlum, string cursoMatAlum, 
            double notaPracticaAlum, double notaActitudAlum, 
            double examenParcialAlum, double promFinalAlum)
            :base( codPersona,  apepatPersona,
             apematPersona, nomPersona,
            sexoPersona,  dniPersona,
            yearOldPersona,edadPersona)
        {
            this.SemestreAlum = semestreAlum;
            this.CursoMatAlum = cursoMatAlum;
            this.NotaPracticaAlum = notaPracticaAlum;
            this.NotaActitudAlum = notaActitudAlum;
            this.ExamenParcialAlum = examenParcialAlum;
            this.PromFinalAlum = promFinalAlum;
        }

        public string SemestreAlum { get => semestreAlum; set => semestreAlum = value; }
        public string CursoMatAlum { get => cursoMatAlum; set => cursoMatAlum = value; }
        public double NotaPracticaAlum { get => notaPracticaAlum; set => notaPracticaAlum = value; }
        public double NotaActitudAlum { get => notaActitudAlum; set => notaActitudAlum = value; }
        public double ExamenParcialAlum { get => examenParcialAlum; set => examenParcialAlum = value; }
        public double PromFinalAlum { get => promFinalAlum; set => promFinalAlum = value; }
    }
}

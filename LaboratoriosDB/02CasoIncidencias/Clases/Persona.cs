﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02CasoIncidencias.Clases
{
    public class Persona
    {
        private string codPersona;
        private string apepatPersona;
        private string apematPersona;
        private string nomPersona;
        private char sexoPersona;
        private string dniPersona;
        private int yearOldPersona;
        private int edadPersona;

        //Constructores
        public Persona(string codPersona, string apepatPersona, string apematPersona, string nomPersona, char sexoPersona, string dniPersona, int yearOldPersona, int edadPersona)
        {
            this.codPersona = codPersona;
            this.apepatPersona = apepatPersona;
            this.apematPersona = apematPersona;
            this.nomPersona = nomPersona;
            this.sexoPersona = sexoPersona;
            this.dniPersona = dniPersona;
            this.yearOldPersona = yearOldPersona;
            this.edadPersona = edadPersona;
        }
        //Encapsulamiento
        public string CodPersona { get => codPersona; set => codPersona = value; }
        public string ApepatPersona { get => apepatPersona; set => apepatPersona = value; }
        public string ApematPersona { get => apematPersona; set => apematPersona = value; }
        public string NomPersona { get => nomPersona; set => nomPersona = value; }
        public char SexoPersona { get => sexoPersona; set => sexoPersona = value; }
        public string DniPersona { get => dniPersona; set => dniPersona = value; }
        public int YearOldPersona { get => yearOldPersona; set => yearOldPersona = value; }
        public int EdadPersona { get => edadPersona; set => edadPersona = value; }

        //Metodos
        public string generarCodigo()
        {

            return null;
        }
        public int obtenerEdad()
        {
            return 0;
        }

    }
}


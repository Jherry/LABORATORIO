﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02CasoIncidencias.Clases
{
    public class Instructor:Persona
    {
        private string especialidadInst;
        private string tipoContratoInst;
        private int horasSemanaInst;
        private double salarioHoraInst;
        private double escolaridadInst;
        private double sueldoTotalInst;

        public Instructor(string codPersona, string apepatPersona,
            string apematPersona, string nomPersona,
            char sexoPersona, string dniPersona,
            int yearOldPersona, int edadPersona, 
            string especialidadInst, string tipoContratoInst, 
            int horasSemanaInst, double salarioHoraInst, 
            double escolaridadInst, double sueldoTotalInst)
            :base(codPersona,  apepatPersona,
            apematPersona,  nomPersona,
            sexoPersona,  dniPersona,
             yearOldPersona,  edadPersona)
        {
            this.EspecialidadInst = especialidadInst;
            this.TipoContratoInst = tipoContratoInst;
            this.HorasSemanaInst = horasSemanaInst;
            this.SalarioHoraInst = salarioHoraInst;
            this.EscolaridadInst = escolaridadInst;
            this.SueldoTotalInst = sueldoTotalInst;
        }

        public string EspecialidadInst { get => especialidadInst; set => especialidadInst = value; }
        public string TipoContratoInst { get => tipoContratoInst; set => tipoContratoInst = value; }
        public int HorasSemanaInst { get => horasSemanaInst; set => horasSemanaInst = value; }
        public double SalarioHoraInst { get => salarioHoraInst; set => salarioHoraInst = value; }
        public double EscolaridadInst { get => escolaridadInst; set => escolaridadInst = value; }
        public double SueldoTotalInst { get => sueldoTotalInst; set => sueldoTotalInst = value; }
    }

        
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Incidencias.Clases
{
    public class Instructor : Persona
    {
        private string especialidadInst;
        private int tipoContratoInst;
        private double sueldoInst;
        private int horasSemanaInst;

        public Instructor(string codPersona, string apepatPersona, string apematPersona, string dniPersona, char sexoPersona, DateTime fecNacPersona, byte fotoPersona, string especialidadInst, int tipoContratoInst, double sueldoInst, int horasSemanaInst)
            : base(codPersona, apepatPersona, apematPersona, dniPersona, sexoPersona, fecNacPersona, fotoPersona)
        {
            this.EspecialidadInst = especialidadInst;
            this.TipoContratoInst = tipoContratoInst;
            this.SueldoInst = sueldoInst;
            this.HorasSemanaInst = horasSemanaInst;
        }

        public string EspecialidadInst { get => especialidadInst; set => especialidadInst = value; }
        public int TipoContratoInst { get => tipoContratoInst; set => tipoContratoInst = value; }
        public double SueldoInst { get => sueldoInst; set => sueldoInst = value; }
        public int HorasSemanaInst { get => horasSemanaInst; set => horasSemanaInst = value; }
    }
}

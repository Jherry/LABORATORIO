﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Incidencias.Clases
{
    public class Persona
    {
        private string codPersona;
        private string apepatPersona;
        private string apematPersona;
        private string dniPersona;
        private char sexoPersona;
        private DateTime fecNacPersona;
        private Byte fotoPersona;

        public Persona(string codPersona, string apepatPersona, string apematPersona, string dniPersona, char sexoPersona, DateTime fecNacPersona, byte fotoPersona)
        {
            this.CodPersona = codPersona;
            this.ApepatPersona = apepatPersona;
            this.ApematPersona = apematPersona;
            this.DniPersona = dniPersona;
            this.SexoPersona = sexoPersona;
            this.FecNacPersona = fecNacPersona;
            this.FotoPersona = fotoPersona;
        }

        public string CodPersona { get => codPersona; set => codPersona = value; }
        public string ApepatPersona { get => apepatPersona; set => apepatPersona = value; }
        public string ApematPersona { get => apematPersona; set => apematPersona = value; }
        public string DniPersona { get => dniPersona; set => dniPersona = value; }
        public char SexoPersona { get => sexoPersona; set => sexoPersona = value; }
        public DateTime FecNacPersona { get => fecNacPersona; set => fecNacPersona = value; }
        public byte FotoPersona { get => fotoPersona; set => fotoPersona = value; }
    }
}
